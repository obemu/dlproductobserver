﻿using System.Windows.Forms;

namespace DLProductObserverControls {
    public partial class NoFocusCueTextBox : TextBox {

        public NoFocusCueTextBox() {
            InitializeComponent();
            SetStyle(ControlStyles.Selectable, false);
        }

        protected override bool ShowFocusCues {
            get => false;
        }
    }
}
