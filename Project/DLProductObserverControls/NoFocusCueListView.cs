﻿using System.Windows.Forms;

namespace DLProductObserverControls {
    public partial class NoFocusCueListView : ListView {

        public NoFocusCueListView() {
            InitializeComponent();
            SetStyle(ControlStyles.Selectable, false);
        }

        protected override bool ShowFocusCues {
            get => false;
        }
    }
}
