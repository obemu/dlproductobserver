﻿using System.Windows.Forms;

namespace DLProductObserverControls {
    public partial class NoFocusCueButton : Button {

        public NoFocusCueButton() {
            InitializeComponent();
            SetStyle(ControlStyles.Selectable, false);
        }

        protected override bool ShowFocusCues {
            get => false;
        }
    }
}
