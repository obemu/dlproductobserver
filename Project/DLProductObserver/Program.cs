﻿using DLProductObserver.Backend;
using DLProductObserver.Backend.WMI;
using System;
using System.Windows.Forms;

using System.IO;

using DLProductObserver.Backend.Excel;
using Microsoft.Win32;
using DLProductObserver.Backend.Database;

namespace DLProductObserver {
    static class Program {

        #region Private Declarations

        private static readonly string _LOG_TAG = nameof(Program);

        private static UI.MainPage _mainPage;
        private static bool _isClosed;

        #endregion

        [STAThread]
        static void Main() {
            _isClosed = true;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try {
                // See: https://stackoverflow.com/a/1409378
                var dataPath = Utils.combineWithUserAppDataPath("data");
                Directory.CreateDirectory(dataPath);
                AppDomain.CurrentDomain.SetData("DataDirectory", dataPath);

                var sourceDBPath = Path.Combine(
                    Utils.getInstallationDirectoryPath(),
                    "ScannedProductsDatabase.mdf"
                    );
                var dstDBPath = Path.Combine(
                    dataPath,
                    "ScannedProductsDatabase.mdf"
                    );

                Log.d(_LOG_TAG,
                    "sourceDBPath: " + sourceDBPath,
                    "dstDBPath: " + dstDBPath,
                    "dataPath: " + dataPath
                    );

                if (!File.Exists(dstDBPath))
                    File.Copy(sourceDBPath, dstDBPath);

            } catch (Exception e) {
                Log.e(_LOG_TAG, e.Message);
            }

            if (!ExcelUtils.isExcelInstalled()) {
                Utils.showErrorMessage("DLProductObserver", "MS Excel is not installed!");
                Application.Exit();
                return;
            }

            Log.initialize();
            Log.d(_LOG_TAG, "ExcelUtils.initialize: " + ExcelUtils.initialize());
            ScannedProductsDB.initialize();

            _mainPage = new UI.MainPage();
            _mainPage.barcodeScanned += mainPage_barcodeScanned;
            _mainPage.FormClosed += _mainPage_FormClosed;
            Application.ApplicationExit += (_, __) => _close();
            SystemEvents.SessionEnded += (_, __) => _close();

            _isClosed = false;

            Application.Run(_mainPage);
        }


        #region Private Methods

        private static void _close() {
            if (_isClosed)
                return;

            _isClosed = true;

            Settings.save();
            ExcelUtils.dispose();
            Log.dispose();
        }

        #region Eventlistener

        private static void _mainPage_FormClosed(object _, FormClosedEventArgs __) {
            if (!string.IsNullOrEmpty(Settings.outputDirectoryPath))
                new ProductWorkbooksExporter(
                    Settings.outputDirectoryPath,
                    ProductWorkbooksExporter.SheetFormat.ExcelSpreadSheet
                    ).export(
                    ScannedProductsDB.getData()
                    );

            _close();
        }

        private static void mainPage_barcodeScanned(object _, Backend.Model.ProductEventArgs e) {
            if (0 == Settings.deviceEntities.Count)
                return;

#if DEBUG
            PNPEntity keyboardEntity =
                (null != e.keyboardDevice)
                ? Utils.getPNPEntityFromKeyboardDevice(e.keyboardDevice)
                : null;
#else
            PNPEntity keyboardEntity = Utils.getPNPEntityFromKeyboardDevice(e.keyboardDevice);
#endif

            Log.i(nameof(mainPage_barcodeScanned),
                "Product: " + e.productEvent.product,
                "Time: " + e.productEvent.timepoint,
                "KeyboardDevice: " + e.keyboardDevice
                );

#if DEBUG
            var deviceEntity = (null != keyboardEntity)
                 ? Settings.deviceEntities.find(keyboardEntity)
                 : null;

            if (null == deviceEntity) {
                Log.i(nameof(mainPage_barcodeScanned),
                    $"DeviceEntity of keyboardEntity \"{keyboardEntity}\" is null."
                    );
            }

            Log.i(nameof(mainPage_barcodeScanned),
                "Adding " + e.productEvent.product + " from device: " + keyboardEntity);
            ScannedProductsDB.add(e);
#else
            var deviceEntity = Settings.deviceEntities.find(keyboardEntity);

            if (null == deviceEntity) {
                Log.i(nameof(mainPage_barcodeScanned),
                    "DeviceEntity of keyboardEntity \"" + keyboardEntity + "\" is null."
                    );
                return;
            }

            if (deviceEntity.isEnabled) {
                Log.i(nameof(mainPage_barcodeScanned), "Adding " + e.productEvent.product + " from device: " + keyboardEntity);
                ScannedProductsDB.add(e);
            }
#endif
        }

        #endregion

        #endregion
    }
}
