using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Collections.Generic;
using DLProductObserver.Backend;
using System.Text;

// Code from https://www.codeproject.com/articles/9351/background-applications-listening-for-keyboard-act

namespace KbdListener {
    #region Class Definition WindowsMessage

    /// <summary>
    /// Windows Messages
    /// Defined in winuser.h from Windows SDK v6.1
    /// Documentation pulled from MSDN.
    /// <br></br>
    /// <see href="http://www.pinvoke.net/default.aspx/Enums.WindowsMessages"/>
    /// <br></br>
    /// <seealso href="http://www.pinvoke.net/default.aspx/Constants/WM.html"/>
    /// </summary>
    public static class WindowsMessage {
        /// <summary>
        /// The WM_KEYDOWN message is posted to the window with the keyboard focus
        /// when a nonsystem key is pressed. A nonsystem key is a key that is
        /// pressed when the ALT key is not pressed. 
        /// </summary>
        public const uint KEYDOWN = 0x0100;

        /// <summary>
        /// The WM_KEYUP message is posted to the window with the keyboard focus
        /// when a nonsystem key is released. A nonsystem key is a key that
        /// is pressed when the ALT key is not pressed, or a keyboard key that
        /// is pressed when a window has the keyboard focus.
        /// </summary>
        public const uint KEYUP = 0x0101;
    }

    #endregion

    /// <summary>
    /// The KeyboardListener is a static class that allows registering a number
    /// of event handlers that you want to get called in case some keyboard key is pressed 
    /// or released. The nice thing is that this KeyboardListener is also active in case
    /// the parent application is running in the back.
    /// </summary>
    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "SkipVerification")]
    public static class KeyboardListener {
        #region Unsafe types

        /// <summary>
        /// Value type for raw input devices.
        /// <br></br>
        /// <see href="http://www.pinvoke.net/default.aspx/Structures.RAWINPUTDEVICE"/>
        /// </summary>
        unsafe struct RAWINPUTDEVICE {
            /// <summary>
            /// Top level collection Usage page for the raw input device.
            /// <br></br>
            /// <see href="https://www.pinvoke.net/default.aspx/Enums/HIDUsagePage.html"/>
            /// </summary>
            public ushort hidUsagePage;

            /// <summary>
            /// Top level collection Usage for the raw input device. 
            /// <br></br>
            /// <see href="https://www.pinvoke.net/default.aspx/Enums/HIDUsage.html"/>
            /// </summary>
            public ushort hidUsage;

            /// <summary>
            /// Mode flag that specifies how to interpret the information provided by UsagePage and Usage.
            /// <br></br>
            /// <see href="https://www.pinvoke.net/default.aspx/Enums/RawInputDeviceFlags.html"/>
            /// </summary>
            public uint flags;

            /// <summary>
            /// Handle to the target device. If NULL, it follows the keyboard focus.
            /// </summary>
            public void* windowHandle;
        };


        /// <summary>
        /// Value type for a raw input header.
        /// <br></br>
        /// <see href="http://pinvoke.net/default.aspx/Structures.RAWINPUTHEADER"/>
        /// <br></br>
        /// <seealso href="https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-rawinputheader"/>
        /// </summary>
        unsafe struct RAWINPUTHEADER {
            /// <summary>
            /// Type of device the input is coming from.
            /// <br></br>
            /// <see href="https://www.pinvoke.net/default.aspx/Enums/RawInputType.html"/>
            /// </summary>
            public uint rawInputType;


            /// <summary>
            /// Size of the packet of data.
            /// </summary>
            public uint dataSize;

            /// <summary>
            /// Handle to the device sending the data.
            /// </summary>
            public void* handleSenderDevice;

            /// <summary>
            /// wParam from the window message.
            /// </summary>
            public void* wParam;
        };

        /// <summary>
        /// Value type for raw input from a keyboard.
        /// <br></br>
        /// <see href="http://pinvoke.net/default.aspx/Structures.RAWINPUTKEYBOARD"/>
        /// <seealso href="https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-rawkeyboard"/>
        /// </summary>
        unsafe struct RAWINPUTKEYBOARD {

            public RAWINPUTHEADER header;

            /// <summary>
            /// Scan code for key depression.
            /// </summary>
            public ushort MakeCode;

            /// <summary>
            /// Scan code information.
            /// <br></br>
            /// <see href="http://www.pinvoke.net/default.aspx/Enums.RawKeyboardFlags"/>
            /// </summary>
            public ushort Flags;

            /// <summary>
            /// Reserved.
            /// </summary>
            public ushort Reserved;

            /// <summary>
            /// Virtual key code.
            /// <br></br>
            /// <see href="http://www.pinvoke.net/default.aspx/Enums.VirtualKeys"/>
            /// </summary>
            public ushort virtualKey;

            /// <summary>
            /// Corresponding window message.
            /// <br></br>
            /// <see href="http://www.pinvoke.net/default.aspx/Enums.WindowsMessages"/>
            /// </summary>
            public uint Message;

            /// <summary>
            /// Extra information.
            /// </summary>
            public uint ExtraInformation;

        };

        #endregion

        #region Private Declarations

        /// <summary>
        /// The Window that intercepts Keyboard messages
        /// </summary>
        private static _ListeningWindow _listener;

        #endregion

        #region Public Declarations

        /// <summary>
        /// For every application thread that is interested in keyboard events
        /// an EventHandler can be added to this variable
        /// </summary>
        public static event EventHandler keyEventHandler;

        #endregion

        static KeyboardListener() {
            _ListeningWindow.KeyDelegate aKeyDelegate = new _ListeningWindow.KeyDelegate(_keyHandler);
            _listener = new _ListeningWindow(aKeyDelegate);
        }

        #region Private Methods

        /// <summary>
        /// The function that will handle all keyboard activity signaled by the ListeningWindow.
        /// In this context handling means calling all registered subscribers for every key pressed / released.
        /// </summary>
        /// <remarks>
        /// Inside this method the events could also be fired by calling
        /// s_KeyEventHandler(null,new KeyEventArgs(key,msg)) However, in case one of the registered
        /// subscribers throws an exception, execution of the non-executed subscribers is cancelled.
        /// </remarks>
        /// <param name="key"></param>
        /// <param name="msg"></param>
        private static void _keyHandler(ushort key, uint msg, ulong keyboardHandle) {
            if (keyEventHandler != null) {
                Delegate[] delegates = keyEventHandler.GetInvocationList();

                foreach (Delegate del in delegates) {
                    EventHandler sink = (EventHandler)del;

                    try {
                        // This is a static class, therefore null is passed as the object reference
                        sink(null, new UniversalKeyEventArgs(key, msg, new KeyboardDevice(keyboardHandle)));
                    }

                    // You can add some meaningful code to this catch block.
                    catch { };
                }
            }
        }

        #region External Methods

        // In case you want to have a comprehensive overview of calling conventions follow the next link:
        // http://www.codeproject.com/cpp/calling_conventions_demystified.asp


        /// <summary>
        /// Function to register a raw input device.
        /// <br></br>
        /// <see href="http://www.pinvoke.net/default.aspx/user32.RegisterRawInputDevices"/>
        /// </summary>
        /// <param name="pRawInputDevices">Array of raw input devices.</param>
        /// <param name="uiNumDevices">Number of devices.</param>
        /// <param name="cbSize">Size of the RAWINPUTDEVICE structure.</param>
        /// <returns>TRUE if successful, FALSE if not.</returns>
        [DllImport("User32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern unsafe bool RegisterRawInputDevices(
                RAWINPUTDEVICE* rawInputDevices,
                uint numDevices,
                uint size
            );


        /// <summary>
        /// Function to retrieve raw input data.
        /// <br></br>
        /// <see href="http://www.pinvoke.net/default.aspx/user32.GetRawInputData"/>
        /// <br></br>
        /// <seealso href="https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getrawinputdata"/>
        /// </summary>
        /// <param name="hRawInput">Handle to the raw input.</param>
        /// <param name="uiCommand">Command to issue when retrieving data.</param>
        /// <param name="pData">Raw input data.</param>
        /// <param name="pcbSize">Number of bytes in the array.</param>
        /// <param name="cbSizeHeader">Size of the header.</param>
        /// <returns>
        /// If pData is NULL and the function is successful, the return value is 0.
        /// If pData is not NULL and the function is successful,
        /// the return value is the number of bytes copied into pData.
        /// <br></br>
        /// If there is an error, the return value is (UINT)-1.
        /// </returns>
        [DllImport("User32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.I4)]
        static extern unsafe int GetRawInputData(
                void* hRawInput,
                uint uiCommand,
                byte* pData,
                uint* pcbSize,
                uint cbSizeHeader
            );


        /// <summary>
        /// Retrieves information about the raw input device.
        /// <br></br>
        /// <see href="http://pinvoke.net/default.aspx/user32.GetRawInputDeviceInfo"/>
        /// <seealso href="https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getrawinputdeviceinfoa"/>
        /// </summary>
        /// <param name="hDevice">A handle to the raw input device. This comes from the hDevice member of RAWINPUTHEADER or from GetRawInputDeviceList.</param>
        /// <param name="uiCommand">Specifies what data will be returned in pData. This parameter can be RIDI_PREPARSEDDATA, RIDI_DEVICENAME or RIDI_DEVICEINFO</param>
        /// <param name="pData">A pointer to a buffer that contains the information specified by uiCommand. If uiCommand is RIDI_DEVICEINFO, set the cbSize member of RID_DEVICE_INFO to sizeof(RID_DEVICE_INFO) before calling GetRawInputDeviceInfo.</param>
        /// <param name="pcbSize">The size, in bytes, of the data in pData.</param>
        /// <returns>
        /// If successful, this function returns a non-negative number indicating the number of bytes copied
        /// to pData. If pData is not large enough for the data, the function returns -1. If pData is NULL,
        /// the function returns a value of zero. In both of these cases, pcbSize is set to the minimum
        /// size required for the pData buffer.
        /// </returns>
        [DllImport("User32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern unsafe uint GetRawInputDeviceInfoA(
              void* hDevice,
              uint uiCommand,
              int* pData,
              uint* pcbSize
            );

        #endregion

        #endregion

        #region Class Definition UniversalKeyEventArgs

        /// <summary>
        /// An instance of this class is passed when Keyboard events are fired by the KeyboardListener.
        /// </summary>
        public class UniversalKeyEventArgs : KeyEventArgs {
            public readonly uint message;
            public readonly ushort key;
            public readonly KeyboardDevice keyboardDevice;

            public UniversalKeyEventArgs(ushort key, uint message, KeyboardDevice keyboardDevice) : base((Keys)key) {
                this.message = message;
                this.key = key;
                this.keyboardDevice = keyboardDevice;
            }

            public string keyToString() {
                switch (KeyCode) {
                    case Keys.NumPad0:
                    case Keys.D0:
                        return "0";

                    case Keys.NumPad1:
                    case Keys.D1:
                        return "1";

                    case Keys.NumPad2:
                    case Keys.D2:
                        return "2";

                    case Keys.NumPad3:
                    case Keys.D3:
                        return "3";

                    case Keys.NumPad4:
                    case Keys.D4:
                        return "4";

                    case Keys.NumPad5:
                    case Keys.D5:
                        return "5";

                    case Keys.NumPad6:
                    case Keys.D6:
                        return "6";

                    case Keys.NumPad7:
                    case Keys.D7:
                        return "7";

                    case Keys.NumPad8:
                    case Keys.D8:
                        return "8";

                    case Keys.NumPad9:
                    case Keys.D9:
                        return "9";

                    case Keys.A:
                        return "A";

                    case Keys.B:
                        return "B";

                    case Keys.C:
                        return "C";

                    case Keys.D:
                        return "D";

                    case Keys.E:
                        return "E";

                    case Keys.F:
                        return "F";

                    case Keys.G:
                        return "G";

                    case Keys.H:
                        return "H";

                    case Keys.I:
                        return "I";

                    case Keys.J:
                        return "J";

                    case Keys.K:
                        return "K";

                    case Keys.L:
                        return "L";

                    case Keys.M:
                        return "M";

                    case Keys.N:
                        return "N";

                    case Keys.O:
                        return "O";

                    case Keys.P:
                        return "P";

                    case Keys.Q:
                        return "Q";

                    case Keys.R:
                        return "R";

                    case Keys.S:
                        return "S";

                    case Keys.T:
                        return "T";

                    case Keys.U:
                        return "U";

                    case Keys.V:
                        return "V";

                    case Keys.W:
                        return "W";

                    case Keys.X:
                        return "X";

                    case Keys.Y:
                        return "Y";

                    case Keys.Z:
                        return "Z";

                    default:
                        return "";
                }
            }

            public bool isNumber {
                get {
                    return (
                        (Convert.ToInt32(Keys.D0) <= KeyValue && Convert.ToInt32(Keys.D9) >= KeyValue) ||
                        (Convert.ToInt32(Keys.NumPad0) <= KeyValue && Convert.ToInt32(Keys.NumPad9) >= KeyValue)
                    );
                }
            }

            /// <summary>
            /// Does not recognize Germanic umlaut
            /// </summary>
            public bool isCharacter {
                get {
                    return (Convert.ToInt32(Keys.A) <= KeyValue && Convert.ToInt32(Keys.Z) >= KeyValue);
                }
            }

            public bool isFunctionKey {
                get {
                    return (Convert.ToInt32(Keys.F1) <= KeyValue && Convert.ToInt32(Keys.F24) >= KeyValue);
                }
            }
        }

        #endregion

        #region Class Definition ListeningWindow

        /// <summary>
        /// A ListeningWindow object is a Window that intercepts Keyboard events.
        /// </summary>
        private class _ListeningWindow : NativeWindow {
            #region Constants
            private const int WS_CLIPCHILDREN = 0x0200_0000;

            private const int WM_INPUT = 0x00FF;

            ///<summary>
            /// If set, this enables the caller to receive the input even when the caller is
            /// not in the foreground. Note that WindowHandle must be specified.
            /// <br></br>
            /// <see href="https://www.pinvoke.net/default.aspx/Enums/RawInputDeviceFlags.html"/>
            /// </summary>
            private const int RawInputDeviceFlag_INPUT_SINK = 0x0000_0100;

            /// <summary>
            /// The command type to issue. In this case we only need Input.
            /// <br></br><
            /// <see href="http://www.pinvoke.net/default.aspx/Enums.RawInputCommand"/>
            /// </summary>
            private const int RawInputCommand_INPUT = 0x1000_0003;

            /// <summary>
            /// The type device the raw input is coming from.
            /// In this case only the keyboard constant is defined.
            /// <br></br>
            /// <see href="https://www.pinvoke.net/default.aspx/Enums/RawInputType.html"/>
            /// </summary>
            private const int RawInputType_KEYBOARD = 1;
            #endregion

            #region Declarations

            public delegate void KeyDelegate(ushort key, uint msg, ulong keyboardHandle);

            private uint m_PrevMessage = 0;
            private ushort m_PrevControlKey = 0;
            private KeyDelegate m_KeyHandler = null;
            #endregion

            public _ListeningWindow(KeyDelegate keyHandlerFunction) {
                m_KeyHandler = keyHandlerFunction;

                CreateParams cp = new CreateParams {
                    // Fill in the CreateParams details.
                    Caption = "Hidden window",
                    ClassName = null,
                    X = 0x7FFFFFFF,
                    Y = 0x7FFFFFFF,
                    Height = 0,
                    Width = 0,
                    //cp.Parent = parent.Handle;
                    Style = WS_CLIPCHILDREN
                };

                // Create the actual invisible window
                this.CreateHandle(cp);

                // Register for Keyboard notification
                unsafe {
                    try {
                        RAWINPUTDEVICE myRawDevice = new RAWINPUTDEVICE {
                            hidUsagePage = 0x01, // Generic = 0x01 
                            hidUsage = 0x06,     // Keyboard = 0x06
                            flags = RawInputDeviceFlag_INPUT_SINK,
                            windowHandle = this.Handle.ToPointer()
                        };

                        if (!RegisterRawInputDevices(&myRawDevice, 1, (uint)sizeof(RAWINPUTDEVICE))) {
                            int err = Marshal.GetLastWin32Error();
                            throw new Win32Exception(err, "ListeningWindow::RegisterRawInputDevices");
                        }
                    } catch { throw; }
                }
            }

            #region Private/Protected methods
            protected override void WndProc(ref Message m) {
                switch (m.Msg) {
                    case WM_INPUT: {
                        try {
                            unsafe {
                                uint bufferSize;
                                uint receivedBytes;
                                uint sizeof_RAWINPUTHEADER = (uint)(sizeof(RAWINPUTHEADER));

                                // Find out the size of the buffer we have to provide
                                int res = GetRawInputData(m.LParam.ToPointer(), RawInputCommand_INPUT, null, &bufferSize, sizeof_RAWINPUTHEADER);

                                // Check if GetRawInputData was successful
                                if (0 == res) {
                                    // Allocate a buffer and ...
                                    byte* lpb = stackalloc byte[(int)bufferSize];

                                    // ... get the data
                                    receivedBytes = (uint)GetRawInputData((RAWINPUTKEYBOARD*)(m.LParam.ToPointer()), RawInputCommand_INPUT, lpb, &bufferSize, sizeof_RAWINPUTHEADER);

                                    if (receivedBytes == bufferSize) {
                                        RAWINPUTKEYBOARD* keybData = (RAWINPUTKEYBOARD*)lpb;

                                        // Finally, analyze the data
                                        if (RawInputType_KEYBOARD == keybData->header.rawInputType) {
                                            if ((m_PrevControlKey != keybData->virtualKey) || (m_PrevMessage != keybData->Message)) {
                                                m_PrevControlKey = keybData->virtualKey;
                                                m_PrevMessage = keybData->Message;

                                                ulong handle = (ulong)keybData->header.handleSenderDevice;

                                                // Call the delegate in case data satisfies
                                                m_KeyHandler(keybData->virtualKey, keybData->Message, handle);
                                            }
                                        }
                                    } else {
                                        string errMsg = string.Format("WndProc::GetRawInputData (2) received {0} bytes while expected {1} bytes", receivedBytes, bufferSize);
                                        throw new Exception(errMsg);
                                    }
                                } else {
                                    string errMsg = string.Format("WndProc::GetRawInputData (1) returned non zero value ({0})", res);
                                    throw new Exception(errMsg);
                                }
                            }
                        } catch { throw; }
                    }
                    break;
                }

                // In case you forget this you will run into problems
                base.WndProc(ref m);
            }

            #endregion
        }

        #endregion

        #region Class Definition KeyboardDevice

        public class KeyboardDevice {

            #region Private Declarations

            /// <summary>
            /// <see href="https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getrawinputdeviceinfoa"/>
            /// </summary>
            private const uint RIDI_DEVICENAME = 0x20000007;

            private string logTag {
                get => "KeyboardDevice." + handle;
            }

            #endregion

            #region Public Declarations

            public UIntPtr handle {
                get; private set;
            }

            #endregion

            public KeyboardDevice(UIntPtr handle) {
                this.handle = handle;
            }

            public KeyboardDevice(ulong handle) : this(new UIntPtr(handle)) { }

            unsafe public KeyboardDevice(void* handle) : this(new UIntPtr(handle)) { }

            #region Public Methods

            public static bool operator ==(KeyboardDevice l, KeyboardDevice r) {
                if (null == (l as object) && null == (r as object))
                    return true;

                if (null == (l as object) && null != (r as object))
                    return false;

                return l.Equals(r);
            }

            public static bool operator !=(KeyboardDevice l, KeyboardDevice r) => !(l == r);

            public override bool Equals(object obj) {
                if (GetType() != obj?.GetType())
                    return false;

                return Equals(obj as KeyboardDevice);
            }

            public override int GetHashCode() => null == handle ? base.GetHashCode() : handle.GetHashCode();

            public override string ToString() => @"{KeyboardDevice: [handle: " + handle + "]}";

            /// <summary>
            /// Get the "Device instance path" property of this KeyboardDevice.
            /// <br></br>
            /// <br></br>
            /// This is equal to the DeviceID property of the Win32_PnPEntity class.
            /// <br></br>
            /// <see href="https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-pnpentity"/>
            /// </summary>
            /// <param name="replaceHash">If true, replace '#' with '\' .</param>
            public string getDeviceID(bool replaceHash = true) {
                if (0 == handle.ToUInt64())
                    return null;

                StringBuilder builder = new StringBuilder();

                unsafe {
                    uint bufferSize;
                    uint result;

                    // if pData is a null pointer, 0 is returned, but bufferSize is
                    // set to the minimum size required for the pData buffer.
                    result = GetRawInputDeviceInfoA(handle.ToPointer(), RIDI_DEVICENAME, null, &bufferSize);

                    if (0 == result) {
                        int* pData = stackalloc int[(int)bufferSize];
                        uint copiedBytes;

                        copiedBytes = GetRawInputDeviceInfoA(handle.ToPointer(), RIDI_DEVICENAME, pData, &bufferSize);

                        if (-1 != (int)copiedBytes && 0 != copiedBytes) {
                            builder.Append(Encoding.UTF8.GetString((byte*)pData, (int)copiedBytes));
                        } else {
                            Log.e(logTag, "Could not retrieve the input device info, GetRawInputDeviceInfoA returned " + copiedBytes);
                        }
                    } else {
                        Log.e(logTag, "Could not retrieve the required buffersize for pData, GetRawInputDeviceInfoA returned " + result);
                    }
                }

                string deviceId;
                if (replaceHash) {
                    deviceId = builder.Replace('#', '\\').ToString();
                } else {
                    deviceId = builder.ToString();
                }
                deviceId = deviceId.ToUpper().Substring(0, deviceId.IndexOf('{') - 1).Substring(4);

                return deviceId;
            }

            public bool Equals(KeyboardDevice other) {
                if (null == (other as object))
                    return false;

                if (!other.handle.Equals(handle))
                    return false;

                return true;
            }

            #endregion
        }

        #endregion
    }
}
