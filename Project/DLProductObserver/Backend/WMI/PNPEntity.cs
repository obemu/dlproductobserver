﻿using System;
using System.Collections.Generic;
using System.Management;
using Newtonsoft.Json;

namespace DLProductObserver.Backend.WMI {

    /// <summary>
    /// <see href="https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-pnpentity"/> 
    /// </summary>
    public class PNPEntity {

        #region Public Declarations

        [JsonIgnore]
        public readonly uint Availability;

        [JsonIgnore]
        public readonly string Caption;

        public readonly string ClassGuid;

        [JsonIgnore]
        public readonly string[] CompatibleID;

        [JsonIgnore]
        public readonly uint ConfigManagerErrorCode;

        [JsonIgnore]
        public readonly bool ConfigManagerUserConfig;

        [JsonIgnore]
        public readonly string CreationClassName;

        [JsonIgnore]
        public readonly string Description;

        public readonly string DeviceID;

        [JsonIgnore]
        public readonly bool ErrorCleared;

        [JsonIgnore]
        public readonly string ErrorDescription;

        [JsonIgnore]
        public readonly string[] HardwareID;

        /// <summary>
        /// Not including this parameter, because it is always empty.
        /// </summary>
        // public readonly datetime InstallDate;

        [JsonIgnore]
        public readonly uint LastErrorCode;

        [JsonIgnore]
        public readonly string Manufacturer;

        [JsonIgnore]
        public readonly string Name;

        [JsonIgnore]
        public readonly string PNPClass;

        [JsonIgnore]
        public readonly string PNPDeviceID;

        [JsonIgnore]
        public readonly uint[] PowerManagementCapabilities;

        [JsonIgnore]
        public readonly bool PowerManagementSupported;

        [JsonIgnore]
        public readonly bool Present;

        [JsonIgnore]
        public readonly string Service;

        [JsonIgnore]
        public readonly string Status;

        [JsonIgnore]
        public readonly uint StatusInfo;

        [JsonIgnore]
        public readonly string SystemCreationClassName;

        [JsonIgnore]
        public readonly string SystemName;

        /// <summary>
        /// Uses the DeviceID property for retrieving each individual id.
        /// <br></br>
        /// 
        /// In most cases, Windows formats the USB device path as follows:
        /// 
        /// HID\vid_vvvv&pid_pppp\ssss\{gggggggg-gggg-gggg-gggg-gggggggggggg}
        /// 
        /// 
        /// Where:
        /// vvvv is the USB vendor ID represented in 4 hexadecimal characters.
        /// pppp is the USB product ID represented in 4 hexadecimal characters.
        /// ssss is the USB serial string represented in n characters. This
        /// value is affected by the USB port the PNPEntity may be connected by.
        /// gggggggg-gggg-gggg-gggg-gggggggggggg is the device interface GUID that is
        /// used to link applications to device with specific drivers loaded. 
        /// 
        /// 
        /// The key for the vendor ID, if there is one, it would be "VID".
        /// The key for the product ID, if there is one, it would be "PID".
        /// 
        /// 
        /// <see href="https://www.silabs.com/community/interface/knowledge-base.entry.html/2013/11/21/windows_usb_devicep-aGxD"/>
        /// </summary>
        [JsonIgnore]
        public Dictionary<string, string> ids {
            get {
                if (null == DeviceID)
                    return null;

                string[] splitId = DeviceID.Split('\\')[1].Split('&');
                var _ids = new Dictionary<string, string>(splitId.Length);
                var keys = new List<string> {
                    "VID",
                    "PID",
                    "MI",
                    "COL",
                };

                foreach (string id in splitId) {

                    foreach (var key in keys) {
                        if (id.Length < key.Length)
                            continue;

                        if (key == id.Substring(0, key.Length)) {
                            var start = key.Length;
                            if (id.Length > key.Length && '_' == id[start])
                                start++;

                            _ids.Add(key, id.Substring(start));
                        }
                    }
                }

                return _ids;
            }
        }

        #endregion

        public PNPEntity(uint Availability, string Caption, string ClassGuid, string[] CompatibleID,
            uint ConfigManagerErrorCode, bool ConfigManagerUserConfig, string CreationClassName, string Description,
            string DeviceID, bool ErrorCleared, string ErrorDescription, string[] HardwareID,
            uint LastErrorCode, string Manufacturer, string Name, string PNPClass,
            string PNPDeviceID, uint[] PowerManagementCapabilities, bool PowerManagementSupported, bool Present,
            string Service, string Status, uint StatusInfo, string SystemCreationClassName,
            string SystemName) {
            this.Availability = Availability;
            this.Caption = Caption;
            this.ClassGuid = ClassGuid;
            this.CompatibleID = CompatibleID;
            this.ConfigManagerErrorCode = ConfigManagerErrorCode;
            this.ConfigManagerUserConfig = ConfigManagerUserConfig;
            this.CreationClassName = CreationClassName;
            this.Description = Description;
            this.DeviceID = DeviceID;
            this.ErrorCleared = ErrorCleared;
            this.ErrorDescription = ErrorDescription;
            this.HardwareID = HardwareID;
            this.LastErrorCode = LastErrorCode;
            this.Manufacturer = Manufacturer;
            this.Name = Name;
            this.PNPClass = PNPClass;
            this.PNPDeviceID = PNPDeviceID;
            this.PowerManagementCapabilities = PowerManagementCapabilities;
            this.PowerManagementSupported = PowerManagementSupported;
            this.Present = Present;
            this.Service = Service;
            this.Status = Status;
            this.StatusInfo = StatusInfo;
            this.SystemCreationClassName = SystemCreationClassName;
            this.SystemName = SystemName;
        }

        public PNPEntity(in ManagementObject pnpEntity) {
            Availability = Convert.ToUInt32(pnpEntity.GetPropertyValue("Availability"));
            Caption = pnpEntity.GetPropertyValue("Caption")?.ToString();
            ClassGuid = pnpEntity.GetPropertyValue("ClassGuid")?.ToString();
            CompatibleID = pnpEntity.GetPropertyValue("CompatibleID") as string[];
            ConfigManagerErrorCode = Convert.ToUInt32(pnpEntity.GetPropertyValue("ConfigManagerErrorCode"));
            ConfigManagerUserConfig = Convert.ToBoolean(pnpEntity.GetPropertyValue("ConfigManagerUserConfig"));
            CreationClassName = pnpEntity.GetPropertyValue("CreationClassName")?.ToString();
            Description = pnpEntity.GetPropertyValue("Description")?.ToString();
            DeviceID = pnpEntity.GetPropertyValue("DeviceID")?.ToString();
            ErrorCleared = Convert.ToBoolean(pnpEntity.GetPropertyValue("ErrorCleared"));
            ErrorDescription = pnpEntity.GetPropertyValue("ErrorDescription")?.ToString();
            HardwareID = pnpEntity.GetPropertyValue("HardwareID") as string[];
            LastErrorCode = Convert.ToUInt32(pnpEntity.GetPropertyValue("LastErrorCode"));
            Manufacturer = pnpEntity.GetPropertyValue("Manufacturer")?.ToString();
            Name = pnpEntity.GetPropertyValue("Name")?.ToString();
            PNPClass = pnpEntity.GetPropertyValue("PNPClass")?.ToString();
            PNPDeviceID = pnpEntity.GetPropertyValue("PNPDeviceID")?.ToString();
            PowerManagementCapabilities = pnpEntity.GetPropertyValue("PowerManagementCapabilities") as uint[];
            PowerManagementSupported = Convert.ToBoolean(pnpEntity.GetPropertyValue("PowerManagementSupported"));
            Present = Convert.ToBoolean(pnpEntity.GetPropertyValue("Present"));
            Service = pnpEntity.GetPropertyValue("Service")?.ToString();
            Status = pnpEntity.GetPropertyValue("Status")?.ToString();
            StatusInfo = Convert.ToUInt32(pnpEntity?.GetPropertyValue("StatusInfo"));
            SystemCreationClassName = pnpEntity.GetPropertyValue("SystemCreationClassName")?.ToString();
            SystemName = pnpEntity.GetPropertyValue("SystemName")?.ToString();
        }

        [JsonConstructor]
        public PNPEntity(string ClassGuid, string DeviceID) {
            this.ClassGuid = ClassGuid;
            this.DeviceID = DeviceID;

            if (!Utils.pnpEntities.ContainsKey(this.DeviceID))
                Utils.fetchPNPEntities();

            if (!Utils.pnpEntities.ContainsKey(this.DeviceID))
                return;

            PNPEntity entity = Utils.pnpEntities[this.DeviceID];

            Availability = entity.Availability;
            Caption = entity.Caption;
            Utils.copyArray(entity.CompatibleID, out CompatibleID);
            ConfigManagerErrorCode = entity.ConfigManagerErrorCode;
            ConfigManagerUserConfig = entity.ConfigManagerUserConfig;
            CreationClassName = entity.CreationClassName;
            Description = entity.Description;
            ErrorCleared = entity.ErrorCleared;
            ErrorDescription = entity.ErrorDescription;
            Utils.copyArray(entity.HardwareID, out HardwareID);
            LastErrorCode = entity.LastErrorCode;
            Manufacturer = entity.Manufacturer;
            Name = entity.Name;
            PNPClass = entity.PNPClass;
            PNPDeviceID = entity.PNPDeviceID;
            Utils.copyArray(entity.PowerManagementCapabilities, out PowerManagementCapabilities);
            PowerManagementSupported = entity.PowerManagementSupported;
            Present = entity.Present;
            Service = entity.Service;
            Status = entity.Status;
            StatusInfo = entity.StatusInfo;
            SystemCreationClassName = entity.SystemCreationClassName;
            SystemName = entity.SystemName;
        }

        #region Public Methods

        public static bool operator ==(PNPEntity l, PNPEntity r) {
            if (null == (l as object) && null == (r as object))
                return true;

            if (null == (l as object) && null != (r as object))
                return false;

            return l.Equals(r);
        }

        public static bool operator !=(PNPEntity l, PNPEntity r) => !(l == r);

        public override string ToString() => JsonConvert.SerializeObject(this);

        public override bool Equals(object obj) {
            if (GetType() != obj?.GetType())
                return false;

            return Equals(obj as PNPEntity);
        }

        public bool Equals(PNPEntity other) {
            if (null == (other as object))
                return false;

            var otherIds = other.ids;
            var thisIds = ids;

            if (otherIds.ContainsKey("VID") && thisIds.ContainsKey("VID")
                && otherIds["VID"] != thisIds["VID"])
                return false;

            if (otherIds.ContainsKey("PID") && thisIds.ContainsKey("PID")
                && otherIds["PID"] != thisIds["PID"])
                return false;


            return true;
        }

        public override int GetHashCode() => HashCode.Combine(ClassGuid, DeviceID);

        #endregion
    }
}
