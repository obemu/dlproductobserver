﻿using System;
using System.Text;
using System.IO;

namespace DLProductObserver.Backend {
    public static class Log {
        #region Private Declarations

        private static readonly string _LOG_DIRECTORY = Utils.combineWithUserAppDataPath("logs");

        private const string _FILE_EXTENSION = ".log";

        private static StreamWriter _streamWriter;

        #endregion

        #region Public Declarations

        // Priority constant for the println method; use Log::v.
        public const byte VERBOSE = 2;

        //  Priority constant for the println method; use Log::d.
        public const byte DEBUG = 3;

        // Priority constant for the println method; use Log::i.
        public const byte INFO = 4;

        //  Priority constant for the println method; use Log::w.
        public const byte WARNING = 5;

        // Priority constant for the println method; use Log::e.
        public const byte ERROR = 6;

        // Setting Log::logLevel to Log::OFF, disables all Log messages.
        public const byte OFF = 7;

        public static byte logLevel = VERBOSE;

        #endregion

        #region Private Methods

        private static string getTimeString() {
            var now = DateTime.Now;
            StringBuilder builder = new StringBuilder($"{now.Year}-");

            if (10 > now.Month)
                builder.Append('0');
            builder.Append($"{now.Month}-");

            if (10 > now.Day)
                builder.Append('0');
            builder.Append($"{now.Day} ");

            if (10 > now.Hour)
                builder.Append('0');
            builder.Append($"{now.Hour}:");

            if (10 > now.Minute)
                builder.Append('0');
            builder.Append($"{now.Minute}:");

            if (10 > now.Second)
                builder.Append('0');
            builder.Append($"{now.Second}.");

            builder.Append(now.Millisecond.ToString("D3"));
            return builder.ToString();
        }

        private static void log(char id, in string tag, in string message) {
            var msg = $"{getTimeString()} {id}/{tag}: {message}";
            Console.WriteLine(msg);
            _streamWriter?.WriteLine(msg);
        }

        #endregion

        #region Public Methods

        public static void initialize() {
            var now = DateTime.Now;
            var todaysLogFilePath = Path.Combine(
                    _LOG_DIRECTORY,
                    Utils.getShortDateString(now, "-") + _FILE_EXTENSION
                );

            Directory.CreateDirectory(_LOG_DIRECTORY);
            _streamWriter = new StreamWriter(todaysLogFilePath, append: true);
        }

        public static void dispose() {
            _streamWriter.Close();
            _streamWriter = null;
        }

        public static void v(string tag, params object[] messages) {
            if (VERBOSE >= logLevel) {
                foreach (var msg in messages)
                    log('V', tag, msg?.ToString());

                _streamWriter?.Flush();
            }
        }

        public static void d(string tag, params object[] messages) {
            if (DEBUG >= logLevel) {
                foreach (var msg in messages)
                    log('D', tag, msg?.ToString());

                _streamWriter?.Flush();
            }
        }

        public static void i(string tag, params object[] messages) {
            if (INFO >= logLevel) {
                foreach (var msg in messages)
                    log('I', tag, msg?.ToString());

                _streamWriter?.Flush();
            }
        }

        public static void w(string tag, params object[] messages) {
            if (WARNING >= logLevel) {
                foreach (var msg in messages)
                    log('W', tag, msg?.ToString());

                _streamWriter?.Flush();
            }
        }

        public static void e(string tag, params object[] messages) {
            if (ERROR >= logLevel) {
                foreach (var msg in messages)
                    log('E', tag, msg?.ToString());

                _streamWriter?.Flush();
            }
        }

        #endregion
    }
}