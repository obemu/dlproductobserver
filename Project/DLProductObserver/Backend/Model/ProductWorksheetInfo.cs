﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLProductObserver.Backend.Model {
    public class ProductWorksheetInfo {
        #region Public Declarations

        public uint currentRow;

        public bool hasTitles {
            get;
        }

        public string name {
            get;
        }

        public Dictionary<Product, uint> productCount {
            get;
        }

        #endregion

        [JsonConstructor]
        public ProductWorksheetInfo(uint currentRow, bool hasTitles, string name, Dictionary<string, uint> productCount) {
            this.currentRow = currentRow;
            this.hasTitles = hasTitles;
            this.name = name;

            this.productCount = new Dictionary<Product, uint>(productCount.Count);

            foreach (var pair in productCount)
                this.productCount.Add(Product.fromJson(pair.Key), pair.Value);
        }

        public ProductWorksheetInfo(uint currentRow, bool hasTitles, string name) {
            this.currentRow = currentRow;
            this.hasTitles = hasTitles;
            this.name = name;
            productCount = new Dictionary<Product, uint>();
        }
    }
}
