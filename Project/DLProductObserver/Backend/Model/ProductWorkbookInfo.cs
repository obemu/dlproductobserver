﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

using Newtonsoft.Json;

namespace DLProductObserver.Backend.Model {
    public class ProductWorkbookInfo {

        #region Public Declarations

        public string workbookName;

        public string workbookDirectory;

        public Dictionary<string, ProductWorksheetInfo> worksheetInfos;

        [JsonIgnore]
        public string workbookPath {
            get => Path.Combine(workbookDirectory, workbookName);
        }

        #endregion

        [JsonConstructor]
        public ProductWorkbookInfo(string workbookName, string workbookDirectory, in Dictionary<string, ProductWorksheetInfo> worksheets) {
            this.workbookName = workbookName;
            this.workbookDirectory = workbookDirectory;
            worksheetInfos = worksheets;
        }

        public ProductWorkbookInfo(string workbookName, string workbookDirectory)
            : this(workbookName, workbookDirectory, new Dictionary<string, ProductWorksheetInfo>()) { }

        public ProductWorkbookInfo(in Excel.ExcelWorkbook workbook, in Dictionary<string, ProductWorksheetInfo> worksheets)
            : this(workbook.name, new FileInfo(workbook.filepath).DirectoryName, worksheets) { }

        public ProductWorkbookInfo(in Excel.ExcelWorkbook workbook)
            : this(workbook.name,
                  new FileInfo(workbook.filepath).DirectoryName,
                  new Dictionary<string, ProductWorksheetInfo>(workbook.worksheetNames.Count)
                  ) {
        }

        public ProductWorkbookInfo(in ProductWorkbookInfo source)
            : this(source.workbookName, source.workbookDirectory, source.worksheetInfos) { }

        #region Public Methods

        public override string ToString() => "{ProductWorkbookInfo: ["
            + "workbookName: " + workbookName + ", "
            + "workbookDirectory: " + workbookDirectory + ", "
            + "worksheets: " + string.Join(",", worksheetInfos.ToArray())
            + "]}";

        #endregion
    }

    public class ProductWorkbookInfoDictionary {
        #region Public Declarations

        public Dictionary<string, ProductWorkbookInfo> workbookInfos {
            get; private set;
        }

        [JsonIgnore]
        public int Count {
            get => workbookInfos.Count;
        }

        #endregion

        public ProductWorkbookInfoDictionary(int capacity) {
            workbookInfos = new Dictionary<string, ProductWorkbookInfo>(capacity);
        }

        [JsonConstructor]
        public ProductWorkbookInfoDictionary(in ProductWorkbookInfoDictionary source) {
            if (null != source) {
                workbookInfos = source.workbookInfos;
            } else {
                workbookInfos = new Dictionary<string, ProductWorkbookInfo>();
            }
        }

        public ProductWorkbookInfoDictionary() {
            workbookInfos = new Dictionary<string, ProductWorkbookInfo>();
        }

        #region Public Methods

        public ProductWorkbookInfo this[string key] {
            get {
                if (!containsKey(key))
                    return null;

                return workbookInfos[key];
            }

            set {
                if (null == value)
                    return;

                if (!containsKey(key))
                    return;

                workbookInfos[key] = value;
            }
        }

        public void add(string key, ProductWorkbookInfo value) {
            if (null == key)
                return;

            if (workbookInfos.ContainsKey(key))
                return;

            workbookInfos.Add(key, value);
        }

        public bool containsValue(ProductWorkbookInfo value) => workbookInfos.ContainsValue(value);

        public bool containsKey(string key) {
            if (null == key)
                return false;

            return workbookInfos.ContainsKey(key);
        }

        #endregion
    }
}
