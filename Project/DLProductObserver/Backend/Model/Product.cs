﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DLProductObserver.Backend.Model {
    public class Product {

        #region Private Declarations

        private const string _LOG_TAG = nameof(Product);

        #endregion

        #region Public Declarations

        public string name;

        public string barcode;

        /// <summary>
        /// The price of this Product saved in the format
        /// x,xx€.
        /// <br></br>
        /// No more than 2 aftercommas are allowed and
        /// zero padding has to be used, e.g. when you want 
        /// to set the price to "5,4" you actually have to set
        /// it to "5,40".
        /// This member is not used for equality checks.
        /// </summary>
        [JsonIgnore]
        public string price {
            get {
                var split = (priceInCent / 100.0).ToString().Split('.');

                if (1 == split.Length)
                    return $"{split[0]},00";

                string zeroPadding = "";
                for (int i = 0; i < (-1 * (split[1].Length - 2)); i++)
                    zeroPadding += '0';

                return $"{split[0]},{split[1]}{zeroPadding}";
            }

            set {
                var split = value.Split(',');

                if (2 != split.Length) {
                    Log.w(_LOG_TAG, "Invalid price: " + price);
                    return;
                }

                if (2 != split[1].Length) {
                    Log.w(_LOG_TAG, "Invalid price: " + price);
                    return;
                }

                priceInCent = Convert.ToInt32(split[0]) * 100 + Convert.ToInt32(split[1]);
            }
        }

        /// <summary>
        /// This member is not used for equality checks.
        /// </summary>
        public int priceInCent;

        #endregion

        [JsonConstructor]
        public Product(string name, string barcode, int priceInCent) {
            this.name = name;
            this.barcode = barcode;
            this.priceInCent = priceInCent;
        }

        public Product(string name, string barcode, string price) {
            this.name = name;
            this.barcode = barcode;
            this.price = price;
        }

        public Product(in Product source) : this(source.name, source.barcode, source.priceInCent) { }

        #region Public Methods

        // TODO: Remove fromJson
        public static Product fromJson(string rawJson) {
            Product product = null;

            try {
                var obj = JObject.Parse(rawJson);

                product = new Product(
                    obj[nameof(Product)][nameof(name)].ToString(),
                    obj[nameof(Product)][nameof(barcode)].ToString(),
                    obj[nameof(Product)][nameof(priceInCent)].ToString()
                    );
            } catch (Exception e) {
                Log.e(_LOG_TAG, "in fromJson", e.Message);
            }

            return product;
        }

        public static bool operator ==(Product l, Product r) {
            if (null == (l as object) && null == (r as object))
                return true;

            if (null == (l as object) && null != (r as object))
                return false;

            return l.Equals(r);
        }

        public static bool operator !=(Product l, Product r) => !(l == r);

        public override string ToString() =>
            "{" + nameof(Product) + ": {"
              + Utils.addMemberToString(nameof(barcode), barcode)
              + Utils.addMemberToString(nameof(name), name)
              + Utils.addMemberToString(nameof(priceInCent), priceInCent, false)
              + "}}";

        public override bool Equals(object obj) {
            if (GetType() != obj?.GetType())
                return false;

            return Equals(obj as Product);
        }

        public bool Equals(Product other) {
            if (null == (other as object))
                return false;

            if (other.name != name)
                return false;

            if (other.barcode != barcode)
                return false;

            return true;
        }

        public override int GetHashCode() => HashCode.Combine(name, barcode);

        #endregion
    }

    public class ProductList : ListWrapper<Product> {
        #region Public Declarations

        [JsonIgnore]
        public override int Count => itemList.Count;

        #endregion

        public ProductList(int capacity) : base(capacity) { }

        public ProductList(in ProductList source) : base(source) { }

        public ProductList() : base() { }

        #region Public Methods

        public Product find(string barcode) => find(item => barcode.Contains(item.barcode));

        #endregion
    }

    public class ProductEvent {
        #region Public Declarations

        public Product product {
            get; private set;
        }

        /// <summary>
        /// The Datetime.Now object when product was scanned.
        /// </summary>
        public DateTime timepoint {
            get; private set;
        }

        #endregion

        public ProductEvent(Product product, DateTime timepoint) {
            this.product = product;
            this.timepoint = timepoint;
        }
    }

    public class ProductEventArgs : EventArgs {
        #region Public Declarations

        public ProductEvent productEvent {
            get; private set;
        }

        public KbdListener.KeyboardListener.KeyboardDevice keyboardDevice {
            get; private set;
        }

        #endregion

        public ProductEventArgs(ProductEvent productEvent, KbdListener.KeyboardListener.KeyboardDevice keyboardDevice) {
            this.productEvent = productEvent;
            this.keyboardDevice = keyboardDevice;
        }

        public ProductEventArgs(Product product, DateTime timepoint, KbdListener.KeyboardListener.KeyboardDevice keyboardDevice)
            : this(new ProductEvent(product, timepoint), keyboardDevice) {
        }
    }
}
