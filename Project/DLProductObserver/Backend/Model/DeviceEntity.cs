﻿using System;

using DLProductObserver.Backend.WMI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DLProductObserver.Backend.Model {
    public class DeviceEntity {
        #region Private Declarations

        private const string _LOG_TAG = nameof(DeviceEntity);

        #endregion

        #region Public Declarations

        /// <summary>
        /// The name chosen by the user for this DeviceEntity.
        /// May be null.
        /// </summary>
        public string simpleName {
            get; private set;
        }

        /// <summary>
        /// The vendor id of this DeviceEntity.
        /// May be null.
        /// </summary>
        public string vendorId {
            get; private set;
        }

        /// <summary>
        /// The product id of this DeviceEntity.
        /// May be null.
        /// </summary>
        public string productId {
            get; private set;
        }

        /// <summary>
        /// Whether or not this DeviceEntity is enabled.
        /// </summary>
        public bool isEnabled;

        #endregion

        [JsonConstructor]
        public DeviceEntity(string simpleName, string vendorId, string productId, bool isEnabled = false) {
            this.simpleName = simpleName;
            this.vendorId = vendorId;
            this.productId = productId;
            this.isEnabled = isEnabled;
        }

        public DeviceEntity(string simpleName, PNPEntity entity, bool isEnabled = false)
            : this(
                  simpleName,
                  entity.ids.ContainsKey("VID") ? entity.ids["VID"] : null,
                  entity.ids.ContainsKey("PID") ? entity.ids["PID"] : null,
                  isEnabled
                  ) {
        }

        public DeviceEntity(PNPEntity entity, bool isEnabled = false)
            : this(null, entity, isEnabled) { }

        #region Public Methods

        public static bool operator ==(DeviceEntity l, DeviceEntity r) {
            if (null == (l as object) && null == (r as object))
                return true;

            if (null == (l as object) && null != (r as object))
                return false;

            return l.Equals(r);
        }

        public static bool operator !=(DeviceEntity l, DeviceEntity r) => !(l == r);

        public override string ToString() => JsonConvert.SerializeObject(this);

        public override bool Equals(object obj) {
            if (GetType() != obj?.GetType())
                return false;

            return Equals(obj as DeviceEntity);
        }

        public bool Equals(DeviceEntity other) {
            if (null == (other as object))
                return false;

            if (other.vendorId != vendorId)
                return false;

            if (other.productId != productId)
                return false;

            return true;
        }

        public override int GetHashCode() => HashCode.Combine(vendorId, productId);

        #endregion
    }

    public class DeviceEntityList : ListWrapper<DeviceEntity> {

        #region Public Declarations

        [JsonIgnore]
        public override int Count => itemList.Count;

        #endregion

        public DeviceEntityList(int capacity) : base(capacity) { }

        public DeviceEntityList(in DeviceEntityList source) : base(source) { }

        public DeviceEntityList() : base() { }

        #region Public Methods

        public DeviceEntity find(PNPEntity entity) => find((e) => {
            if (null == entity)
                return null == e.vendorId && null == e.productId;

            var vid = entity.ids.ContainsKey("VID") ? entity.ids["VID"] : null;
            var pid = entity.ids.ContainsKey("PID") ? entity.ids["PID"] : null;

            return vid == e.vendorId && pid == e.productId;
        });

        #endregion
    }

}
