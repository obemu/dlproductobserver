﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLProductObserver.Backend.Model {
    public class ColumnInfo {

        #region Public Declarations

        /// <summary>
        /// Column index starting from 1.
        /// </summary>
        public readonly int index;

        public readonly string title;

        public string format;

        public string cellString {
            get => columnIndexToCellString(index);
        }

        #endregion

        public ColumnInfo(int index, string title, string format) {
            this.index = index;
            this.title = title;
            this.format = format;
        }

        public ColumnInfo(int index, string title) : this(index, title, null) {
        }

        public ColumnInfo(int index) : this(index, null, null) {
        }

        #region Public Methods

        public static string columnIndexToCellString(int index) {
            index--; //adjust so it matches 0-indexed array rather than 1-indexed column

            int quotient = index / 26;
            char ch = (char)((char)65 + (index % 26));

            if (quotient > 0) {
                return columnIndexToCellString(quotient) + ch.ToString();
            } else {
                return ch.ToString();
            }
        }

        #endregion
    }
}
