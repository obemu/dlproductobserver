﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DLProductObserver.Backend.Model {
    public abstract class ListWrapper<T> {

        #region Public Declarations

        public List<T> itemList;

        public abstract int Count {
            get;
        }

        #endregion

        public ListWrapper(int capacity) {
            itemList = new List<T>(capacity);
        }

        public ListWrapper(in ListWrapper<T> source) {
            itemList = new List<T>(source.itemList);
        }

        public ListWrapper() {
            itemList = new List<T>();
        }

        #region Public Methods

        public T this[int index] {
            get => itemList[index];
            set => itemList[index] = value;
        }

        public void add(T item) => itemList.Add(item);

        public void add(ListWrapper<T> other) => add(other.itemList);

        public void add(List<T> items) => itemList.AddRange(items);

        public bool contains(T item) => itemList.Contains(item);

        public T find(Predicate<T> match) => itemList.Find(match);

        public void forEach(Action<T> action) => itemList.ForEach(action);

        public List<T>.Enumerator GetEnumerator() => itemList.GetEnumerator();

        public int indexOf(T item) => itemList.IndexOf(item);

        public bool remove(T item) => itemList.Remove(item);

        public void removeAt(int index) => itemList.RemoveAt(index);

        #endregion
    }
}
