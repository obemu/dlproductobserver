﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Management;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DLProductObserver.Backend.WMI;
using Microsoft.Win32;
using static KbdListener.KeyboardListener;

namespace DLProductObserver.Backend {
    public static class Utils {

        #region Public Declarations

        public const string LOG_TAG = nameof(Utils);

        /// <summary>
        /// A Dictionary containing all connected PNPEntities when fetchPNPEntities was called.
        /// <br></br>
        /// The key is the device ID.
        /// </summary>
        public static Dictionary<string, PNPEntity> pnpEntities {
            get; private set;
        }

        /// <summary>
        /// A list containing all device IDs of PNPEntities that are connected by a USB port,
        /// when fetchUSBDeviceIds was called.
        /// </summary>
        public static List<string> usbDeviceIds {
            get; private set;
        }

        #endregion

        static Utils() {
            pnpEntities = fetchPNPEntities();
            usbDeviceIds = fetchUSBDeviceIds();
        }

        #region Public Methods

        public static bool isConnectedByUSBPort(PNPEntity entity) {
            if (null == entity)
                return false;

            Func<bool> find = () => {
                foreach (var deviceId in usbDeviceIds) {
                    if (deviceId == entity.DeviceID)
                        return true;
                }

                return false;
            };

            if (find())
                return true;

            fetchUSBDeviceIds();

            return find();
        }

        /// <summary>
        /// Fetch all currently connected PNPEntities. This function also updates the static pnpEntities Dictionary.
        /// <br></br>
        /// <see href="https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-pnpentity"/>
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, PNPEntity> fetchPNPEntities() {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity");
            Dictionary<string, PNPEntity> fetchedPnpEntities = new Dictionary<string, PNPEntity>();
            string deviceId;

            foreach (ManagementObject property in searcher.Get()) {
                deviceId = property.GetPropertyValue("DeviceID")?.ToString();
                if (null != deviceId && 0 < deviceId.Length)
                    fetchedPnpEntities.Add(deviceId, new PNPEntity(property));
            }

            // update static pnpEntities
            if (null != pnpEntities)
                pnpEntities = new Dictionary<string, PNPEntity>(fetchedPnpEntities);

            return fetchedPnpEntities;
        }

        /// <summary>
        /// Fetch all currently, via USB port, connected device IDs. This function also updates the static usbDeviceIds List.
        /// <br></br>
        /// <see href="https://stackoverflow.com/questions/3331043/get-list-of-connected-usb-devices"/>
        /// <br></br>
        /// <seealso href="https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-usbcontrollerdevice"/>
        /// </summary>
        public static List<string> fetchUSBDeviceIds() {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_USBControllerDevice");
            List<string> fetchedUsbDeviceIds = new List<string>();
            string deviceId;

            foreach (ManagementObject found in searcher.Get()) {
                deviceId = found.GetPropertyValue("Dependent").ToString();
                deviceId = deviceId.Substring(deviceId.IndexOf("DeviceID=") + 9)
                    .Replace(@"\\", @"\")
                    .Replace('"', ' ')
                    .Trim();

                fetchedUsbDeviceIds.Add(deviceId);
            }

            // update static usbDeviceIds
            if (null != usbDeviceIds)
                usbDeviceIds = new List<string>(fetchedUsbDeviceIds);

            return fetchedUsbDeviceIds;
        }

        public static PNPEntity getPNPEntityFromIds(string vid = null, string pid = null) {
            if (null == vid && null == pid)
                return null;

            PNPEntity pnpEntity = null;

            var hasVendorId = null != vid;
            var hasProductId = null != pid;

            var matchingVendorId = false;
            var matchingProductId = false;

            Func<bool> findPNPEntity = () => {
                foreach (var entity in Utils.pnpEntities.Values) {
                    if (hasVendorId && entity.ids.ContainsKey("VID") && entity.ids["VID"] == vid)
                        matchingVendorId = true;

                    if (hasProductId && entity.ids.ContainsKey("PID") && entity.ids["PID"] == pid)
                        matchingProductId = true;

                    if ((hasVendorId && hasProductId) && (matchingVendorId && matchingProductId)) {
                        pnpEntity = entity;
                        return true;
                    } else if ((hasVendorId && !hasProductId) && (matchingVendorId)) {
                        pnpEntity = entity;
                        return true;
                    } else if ((!hasVendorId && hasProductId) && (matchingProductId)) {
                        pnpEntity = entity;
                        return true;
                    }

                    matchingVendorId = false;
                    matchingProductId = false;
                }

                return false;
            };

            if (findPNPEntity())
                return pnpEntity;

            fetchPNPEntities();
            findPNPEntity();

            return pnpEntity;
        }

        public static PNPEntity getPNPEntityFromKeyboardDevice(in KeyboardDevice device) {
            var deviceId = device?.getDeviceID();

            if (null == deviceId)
                return null;

            PNPEntity entity = null;

            try {
                entity = pnpEntities[deviceId];
            } catch (KeyNotFoundException) {
                fetchPNPEntities();

                try {
                    entity = pnpEntities[deviceId];
                } catch (KeyNotFoundException e) {

                    Log.e(LOG_TAG,
                          "KeyNotFoundException in getPNPEntityFromKeyboardDevice",
                          "KeyboardDevice: " + device,
                          "Message: " + e.Message
                         );
                }

            }

            return entity;
        }

        public static string getBackupDirectoryPath() {
            string path = combineWithUserAppDataPath("backup");
            Directory.CreateDirectory(path);
            return path;
        }

        public static string getUserAppDataPath()
            => Path.GetDirectoryName(Application.UserAppDataPath);

        public static string combineWithUserAppDataPath(params string[] paths) {
            string[] pathsWithUserAppData = new string[paths.Length + 1];

            pathsWithUserAppData[0] = getUserAppDataPath();
            Array.Copy(paths, 0, pathsWithUserAppData, 1, paths.Length);

            return Path.Combine(pathsWithUserAppData);
        }

        public static string getMonthName(int month)
            => CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);

        public static string getMonthName(in DateTime dateTime)
            => getMonthName(dateTime.Month);

        public static string getShortMonthName(int month)
            => CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(month);

        public static string getShortMonthName(in DateTime dateTime)
            => getShortMonthName(dateTime.Month);

        /// <summary>
        /// Get a formatted string from datetime.
        /// </summary>
        /// <param name="seperator">The seperator between YYYY and MM and between MM and DD.</param>
        /// <param name="reverse">If true then the format is DDMMYYYY instead of YYYYMMDD.</param>
        /// <returns>A string in the format YYYYMMDD.</returns>
        public static string getShortDateString(
            in DateTime dateTime, string seperator = ".",
            bool reverse = false
            ) {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();

            if (reverse) {
                if (10 > dateTime.Day)
                    builder.Append('0');
                builder.Append(dateTime.Day);
                builder.Append(seperator);

                if (10 > dateTime.Month)
                    builder.Append('0');
                builder.Append(dateTime.Month);
                builder.Append(seperator);

                builder.Append(dateTime.Year);
            } else {
                builder.Append(dateTime.Year);
                builder.Append(seperator);

                if (10 > dateTime.Month)
                    builder.Append('0');
                builder.Append(dateTime.Month);
                builder.Append(seperator);

                if (10 > dateTime.Day)
                    builder.Append('0');
                builder.Append(dateTime.Day);
            }

            return builder.ToString();
        }

        public static string getShortTimeString(
            in DateTime dateTime, string seperator = ":"
            ) {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();

            if (10 > dateTime.Hour)
                builder.Append('0');
            builder.Append(dateTime.Hour);
            builder.Append(seperator);

            if (10 > dateTime.Minute)
                builder.Append('0');
            builder.Append(dateTime.Minute);
            builder.Append(seperator);

            if (10 > dateTime.Second)
                builder.Append('0');
            builder.Append(dateTime.Second);

            return builder.ToString();
        }

        public static string addMemberToString(
            string nameOfMember, object value,
            bool appendComma = true
            )
            => '"' + nameOfMember + '"' + ": " + '"' + value + '"' + (appendComma ? ", " : "");

        public static void copyArray<T>(in T[] source, out T[] destination) {
            if (null == source) {
                destination = null;
                return;
            }

            destination = new T[source.Length];
            Array.Copy(source, destination, source.Length);
        }

        public static DialogResult showErrorMessage(string title, string message)
            => MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);

        public static string getInstallationDirectoryPath()
            => Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().Location
                );

        #endregion
    }
}
