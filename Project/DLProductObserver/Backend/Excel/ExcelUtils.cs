﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Microsoft.Office.Interop.Excel;

using DLProductObserver.Backend;
using System.Runtime.InteropServices;

namespace DLProductObserver.Backend.Excel {

    public sealed class ExcelUtils {
        #region Private Declarations

        private static Application xlApplication;
        private static Workbooks xlWorkbooks;

        #endregion

        #region Public Declarations

        /// <summary>
        /// Placeholder for missing values for Excel parameters.
        /// </summary>
        public static readonly object missing = Type.Missing;

        public const string LOG_TAG = "ExcelUtils";

        #endregion

        #region Private Methods

        #endregion

        #region Public Methods

        public static bool isExcelInstalled() => null != Type.GetTypeFromProgID("Excel.Application");

        public static bool initialize() {
            if (!isExcelInstalled()) {
                Log.e(LOG_TAG, "Excel is not installed!");
                return false;
            }

            xlApplication = new Application();

            if (null == xlApplication) {
                Log.e(LOG_TAG, "Could not initialize xlApplication");
                return false;
            }

            xlWorkbooks = xlApplication.Workbooks;

            return true;
        }

        public static void dispose() {
            if (null == xlApplication)
                return;

            xlApplication.Quit();

            // This is Importent for free memory and excel file.
            // Release the Excel Object as this

            GC.Collect();
            GC.WaitForPendingFinalizers();

            try {
                xlWorkbooks.Close();
                Marshal.FinalReleaseComObject(xlWorkbooks);
                xlWorkbooks = null;
            } catch (Exception e) {
                xlWorkbooks = null;
                Log.e(LOG_TAG, e.ToString());
            } finally {
                GC.Collect();
            }

            try {
                Marshal.ReleaseComObject(xlApplication);
                xlApplication = null;
            } catch (Exception e) {
                xlApplication = null;
                Log.e(LOG_TAG, e.ToString());
            } finally {
                GC.Collect();
            }

            GC.WaitForPendingFinalizers();
        }

        /// <summary>
        /// Close the xlInteropObject and if the cleanup Action is not null, call it before
        /// releasing the xlInteropObject.
        /// <br></br>
        /// After the xlInteropObject has been cleaned up, is set to its default value.
        /// <br></br>
        /// If xlInteropObject is null this function immediately returns.
        /// </summary>
        public static void closeExcelInteropObject<T>(ref T xlInteropObject, Action<T> cleanup = null) {
            if (null == xlInteropObject)
                return;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            try {
                cleanup?.Invoke(xlInteropObject);
                Marshal.FinalReleaseComObject(xlInteropObject);
                xlInteropObject = default;
            } catch (Exception e) {
                xlInteropObject = default;
                Log.e(LOG_TAG, e.ToString());
            } finally {
                GC.Collect();
            }
        }

        public static void getWorkbook(out Workbook xlWorkbook, string filepath) {
            xlWorkbook = null;

            if (null == xlApplication)
                return;

            foreach (Workbook wb in xlWorkbooks) {
                if (Path.Combine(wb.Path, wb.Name) == filepath) {
                    xlWorkbook = wb;
                    return;
                }
            }
        }

        public static bool openWorkbook(string filepath, bool createIfNotExists = true) {
            if (null == xlApplication)
                return false;

            if (!File.Exists(filepath)) {
                if (!createIfNotExists)
                    return false;

                var workbook = xlWorkbooks.Add();
                workbook.SaveAs(filepath, XlFileFormat.xlWorkbookDefault);

                return true;
            }

            xlWorkbooks.Open(filepath);

            return true;
        }

        /// <summary>
        /// Create a new excel workbook.
        /// <br></br>
        /// When creating a new workbook it is automatically
        /// opened, but this behaviour is not desired, so it
        /// gets closed immediately.
        /// </summary>
        public static bool createWorkbook(string filepath) {
            if (null == xlApplication)
                return false;

            if (File.Exists(filepath))
                return true;

            var workbook = xlWorkbooks.Add();
            workbook.SaveAs(filepath, XlFileFormat.xlWorkbookDefault);
            closeExcelInteropObject(ref workbook, (wb) => wb.Close());

            return true;
        }

        // TODO: Implement isWorkbookOpen
        public static bool isWorkbookOpen(string filepath) {
            throw new NotImplementedException();
        }

        // TODO: Implement closeApplication
        /// <summary>
        /// Closes the Excel application in which the workbook with filepath is opened.
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="save"></param>
        public static void closeApplication(string filepath, bool save = false) {
            throw new NotImplementedException();
        }

        #endregion
    }
}
