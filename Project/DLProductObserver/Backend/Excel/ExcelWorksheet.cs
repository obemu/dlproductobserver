﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DLProductObserver.Backend.Model;
using Microsoft.Office.Interop.Excel;

namespace DLProductObserver.Backend.Excel {
    public class ExcelWorksheet {

        #region Private Declarations

        private const string LOG_TAG = nameof(ExcelWorksheet);

        private Worksheet _xlWorksheet;

        #endregion

        #region Public Declarations

        public Range cells => _xlWorksheet.Cells;

        public string name {
            get => _xlWorksheet.Name;
            set => _xlWorksheet.Name = value;
        }

        #endregion

        public ExcelWorksheet(Worksheet worksheet) {
            _xlWorksheet = worksheet;
        }

        ~ExcelWorksheet() {
            ExcelUtils.closeExcelInteropObject(ref _xlWorksheet);
        }

        #region Public Methods

        public void formatCell(int column, int row, string format) {
            var cellColumn = ColumnInfo.columnIndexToCellString(column);
            var range = _xlWorksheet.get_Range($"{cellColumn}:{row}");
            range.NumberFormat = format;
        }

        public void formatColumn(int column, string format) {
            var cellColumn = ColumnInfo.columnIndexToCellString(column);
            var range = _xlWorksheet.get_Range($"{cellColumn}:{cellColumn}");
            range.NumberFormat = format;
        }

        public void formatColumn(in ColumnInfo info) => formatColumn(info.index, info.format);

        public void autoFitRows(int column) {
            var cellColumn = ColumnInfo.columnIndexToCellString(column);
            var range = _xlWorksheet.get_Range($"{cellColumn}:{cellColumn}");
            range.Rows.AutoFit();
        }

        public void autoFitRows(in ColumnInfo info) => autoFitRows(info.index);

        public void setWidth(int column, double width) {
            var cellColumn = ColumnInfo.columnIndexToCellString(column);
            var range = _xlWorksheet.get_Range($"{cellColumn}:{cellColumn}");
            range.ColumnWidth = width;
            ExcelUtils.closeExcelInteropObject(ref range);
        }

        public void setWidth(in ColumnInfo info, double width) => setWidth(info.index, width);

        #endregion
    }
}
