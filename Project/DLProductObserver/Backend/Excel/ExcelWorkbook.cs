﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Office.Interop.Excel;

namespace DLProductObserver.Backend.Excel {
    public class ExcelWorkbook {

        #region Private Declarations

        private Workbook _xlWorkbook;
        private Sheets _xlWorksheets;

        #endregion

        #region Public Declarations

        public string filepath {
            get; private set;
        }

        public bool isOpened {
            get; private set;
        }

        public string name {
            get => _xlWorkbook.Name;
        }

        public ExcelWorksheet activeWorksheet {
            get => new ExcelWorksheet(_xlWorkbook.ActiveSheet);
        }

        public int amountOfWorksheets => _xlWorksheets.Count;

        public List<string> worksheetNames {
            get {
                List<string> names = new List<string>(_xlWorksheets.Count);
                foreach (Worksheet sheet in _xlWorksheets)
                    names.Add(sheet.Name);
                return names;
            }

        }

        #endregion

        public ExcelWorkbook() {
            isOpened = false;
        }

        /// <summary>
        /// Construct a new ExcelWorkbook and call open on filepath.
        /// </summary>
        public ExcelWorkbook(string filepath) {
            this.filepath = filepath;
            open(filepath);
        }

        ~ExcelWorkbook() {
            close();
        }

        #region Public Methods

        public ExcelWorksheet addWorksheet(string name) {
            _xlWorksheets.Add();
            _xlWorkbook.ActiveSheet.Name = name;
            return new ExcelWorksheet(_xlWorkbook.ActiveSheet);
        }

        public ExcelWorksheet getWorksheet(string name) {
            foreach (Worksheet ws in _xlWorksheets) {
                if (ws.Name == name)
                    return new ExcelWorksheet(ws);
            }

            return null;
        }

        public void close(bool save = true) {
            if (!isOpened)
                return;

            isOpened = false;

            ExcelUtils.closeExcelInteropObject(ref _xlWorksheets);
            ExcelUtils.closeExcelInteropObject(ref _xlWorkbook, (wb) => {
                if (save)
                    wb.Save();
                wb.Close();
            });
        }

        /// <summary>
        /// Open an ExcelWorkbook if it exists, else create it and then open it.
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public bool open(string filepath) {
            if (null == this.filepath)
                this.filepath = filepath;

            if (isOpened)
                return true;

            isOpened = ExcelUtils.openWorkbook(filepath);
            ExcelUtils.getWorkbook(out _xlWorkbook, filepath);
            _xlWorksheets = _xlWorkbook.Worksheets;

            return isOpened;
        }

        public void save() => _xlWorkbook?.Save();

        #endregion
    }
}
