﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

using DLProductObserver.Backend.Model;
using Newtonsoft.Json;
using System.Globalization;

namespace DLProductObserver.Backend.Excel {
    public sealed class ProductWorkbook {

        #region Private Declarations

        private const string _LOG_TAG = nameof(ProductWorkbook);

        private const string _SUMMARY_NAME = "Summary";

        private const int _SUMMARY_WORKSHEET_PRODUCT_NAME_COLUMN = 1;
        private const int _SUMMARY_WORKSHEET_PRODUCT_COUNT_COLUMN = 2;
        private const int _SUMMARY_WORKSHEET_PRODUCT_PRICE_COLUMN = 3;

        private const double _WORKSHEET_COLUMN_WIDTH = 11;

        private static readonly ColumnInfo _INFO_TIME_POINT = new ColumnInfo(1, "Zeitpunkt");
        private static readonly ColumnInfo _INFO_PRODUCT_NAME = new ColumnInfo(2, "Produkt");
        private static readonly ColumnInfo _INFO_PRODUCT_PRICE = new ColumnInfo(3, "Preis");

        private static readonly ColumnInfo _INFO_SUMMARY_PRODUCT_NAME = new ColumnInfo(6, "Produkt");
        private static readonly ColumnInfo _INFO_SUMMARY_PRODUCT_COUNT = new ColumnInfo(7, "Anzahl");
        private static readonly ColumnInfo _INFO_SUMMARY_PRODUCT_PRICE = new ColumnInfo(8, "Preis");

        private static readonly object _THREAD_LOCK = new object();

        private static ProductWorkbook _instance = null;
        private static ProductWorkbook instance {
            get {
                lock (_THREAD_LOCK) {
                    if (null == _instance)
                        _instance = new ProductWorkbook();
                    return _instance;
                }
            }
        }

        private ProductWorkbookSettings _workbookSettings;
        private ProductWorkbookInfo _monthlyWorkbookInfo {
            get => _workbookSettings.getInfo(_monthlyWorkbook);
            set {
                if (_workbookSettings.containsInfo(value)) {
                    _workbookSettings.updateInfo(value);
                } else {
                    _workbookSettings.addInfo(value);
                }
            }
        }
        private ProductWorksheetInfo _dailyWorksheetInfoForAddedProducts {
            get {
                if (!_monthlyWorkbookInfo.worksheetInfos.ContainsKey(_dailyWorksheet.name))
                    return null;

                return _monthlyWorkbookInfo.worksheetInfos[_dailyWorksheet.name];
            }
            set {
                if (_monthlyWorkbookInfo.worksheetInfos.ContainsKey(_dailyWorksheet.name)) {
                    _monthlyWorkbookInfo.worksheetInfos[_dailyWorksheet.name] = value;
                    return;
                }

                _monthlyWorkbookInfo.worksheetInfos.Add(_dailyWorksheet.name, value);
            }
        }

        private ExcelWorkbook _monthlyWorkbook;
        private ExcelWorksheet _dailyWorksheet;
        private ExcelWorkbook _summaryWorkbook;
        private ExcelWorksheet _summaryWorksheet;

        private string _workbookDirectory;
        private int _year;
        private string _month;
        private string _yearlyDirectory {
            get => Path.Combine(_workbookDirectory, _year.ToString());
        }

        private bool _isOpened;

        #endregion

        #region Public Declarations

        public static bool isOpened {
            get => instance._isOpened;
        }

        #endregion

        private ProductWorkbook() {
            _isOpened = false;
        }

        ~ProductWorkbook() {
            _isOpened = false;
            close();
            _monthlyWorkbook = null;
            _summaryWorkbook = null;
            _workbookSettings = null;
        }

        #region Private Methods

        private void _updateSummaryWorkbook() {
            //Dictionary<Product, uint> summaryProductCount = new Dictionary<Product, uint>();

            //foreach (var wbInfo in _workbookSettings.workbookInfos.workbookInfos.Values) {
            //    foreach (var wsInfo in wbInfo.worksheetInfos.Values) {
            //        foreach (var pair in wsInfo.productCount) {
            //            if (summaryProductCount.ContainsKey(pair.Key)) {
            //                summaryProductCount[pair.Key] += pair.Value;
            //            } else {
            //                summaryProductCount.Add(pair.Key, pair.Value);
            //            }
            //        }
            //    }
            //}

            //var sheet = _summaryWorkbook.activeWorksheet;
        }

        private void _updateSummaryWorksheet() {
            Dictionary<Product, uint> summaryProductCount = new Dictionary<Product, uint>();

            foreach (var info in _monthlyWorkbookInfo.worksheetInfos.Values) {
                foreach (var pair in info.productCount) {
                    if (summaryProductCount.ContainsKey(pair.Key)) {
                        summaryProductCount[pair.Key] += pair.Value;
                    } else {
                        summaryProductCount.Add(pair.Key, pair.Value);
                    }
                }
            }

            _updateProductSummaryColumns(_summaryWorksheet, summaryProductCount);
        }

        private void _updateProductSummaryColumns(
                ExcelWorksheet worksheet,
                Dictionary<Product, uint> productCount
            ) {
            int nameColumn;
            int countColumn;
            int priceColumn;
            string countColumnCellString;

            if (ReferenceEquals(_summaryWorksheet, worksheet)) {
                nameColumn = _SUMMARY_WORKSHEET_PRODUCT_NAME_COLUMN;
                countColumn = _SUMMARY_WORKSHEET_PRODUCT_COUNT_COLUMN;
                priceColumn = _SUMMARY_WORKSHEET_PRODUCT_PRICE_COLUMN;
                countColumnCellString = ColumnInfo.columnIndexToCellString(_SUMMARY_WORKSHEET_PRODUCT_COUNT_COLUMN);
            } else {
                nameColumn = _INFO_SUMMARY_PRODUCT_NAME.index;
                countColumn = _INFO_SUMMARY_PRODUCT_COUNT.index;
                priceColumn = _INFO_SUMMARY_PRODUCT_PRICE.index;
                countColumnCellString = _INFO_SUMMARY_PRODUCT_COUNT.cellString;
            }

            // first row is reserved for the titles
            int i = 1;
            foreach (var pair in productCount) {
                worksheet.cells[i + 1, nameColumn] = pair.Key.name;
                worksheet.cells[i + 1, countColumn] = pair.Value.ToString();
                worksheet.cells[i + 1, priceColumn] = $"=({countColumnCellString}{i + 1} * {pair.Key.priceInCent}) / 100";
                i++;
            }
        }

        private void _initializeNewDailyWorksheet() {
            // CurrentRow is set to 2 because we start counting at 1 and the 
            // first row contains the titles.
            _dailyWorksheetInfoForAddedProducts = new ProductWorksheetInfo(2, true, _dailyWorksheet.name);

            // Set the titles for each column.
            _dailyWorksheet.cells[1, _INFO_PRODUCT_NAME.index] = _INFO_PRODUCT_NAME.title;
            _dailyWorksheet.cells[1, _INFO_PRODUCT_PRICE.index] = _INFO_PRODUCT_PRICE.title;
            _dailyWorksheet.cells[1, _INFO_TIME_POINT.index] = _INFO_TIME_POINT.title;

            _dailyWorksheet.cells[1, _INFO_SUMMARY_PRODUCT_NAME.index] = _INFO_SUMMARY_PRODUCT_NAME.title;
            _dailyWorksheet.cells[1, _INFO_SUMMARY_PRODUCT_PRICE.index] = _INFO_SUMMARY_PRODUCT_PRICE.title;
            _dailyWorksheet.cells[1, _INFO_SUMMARY_PRODUCT_COUNT.index] = _INFO_SUMMARY_PRODUCT_COUNT.title;

            // Format columns that have a custom format.
            _dailyWorksheet.formatColumn(_INFO_PRODUCT_PRICE);
            _dailyWorksheet.formatColumn(_INFO_SUMMARY_PRODUCT_PRICE);

            // Set the width for all columns
            _dailyWorksheet.setWidth(_INFO_PRODUCT_NAME, _WORKSHEET_COLUMN_WIDTH);
            _dailyWorksheet.setWidth(_INFO_PRODUCT_PRICE, _WORKSHEET_COLUMN_WIDTH);
            _dailyWorksheet.setWidth(_INFO_TIME_POINT, _WORKSHEET_COLUMN_WIDTH);

            _dailyWorksheet.setWidth(_INFO_SUMMARY_PRODUCT_NAME, _WORKSHEET_COLUMN_WIDTH);
            _dailyWorksheet.setWidth(_INFO_SUMMARY_PRODUCT_PRICE, _WORKSHEET_COLUMN_WIDTH);
            _dailyWorksheet.setWidth(_INFO_SUMMARY_PRODUCT_COUNT, _WORKSHEET_COLUMN_WIDTH);
        }

        private void _initializeNewMonthlyWorkbook() {
            _summaryWorksheet.cells[1, _SUMMARY_WORKSHEET_PRODUCT_NAME_COLUMN] = _INFO_SUMMARY_PRODUCT_NAME.title;
            _summaryWorksheet.cells[1, _SUMMARY_WORKSHEET_PRODUCT_COUNT_COLUMN] = _INFO_SUMMARY_PRODUCT_COUNT.title;
            _summaryWorksheet.cells[1, _SUMMARY_WORKSHEET_PRODUCT_PRICE_COLUMN] = _INFO_SUMMARY_PRODUCT_PRICE.title;

            // Format columns that have a custom format.
            _summaryWorksheet.formatColumn(
                    _SUMMARY_WORKSHEET_PRODUCT_PRICE_COLUMN,
                    _INFO_SUMMARY_PRODUCT_PRICE.format
                );

            // Set the width for all columns
            _summaryWorksheet.setWidth(_SUMMARY_WORKSHEET_PRODUCT_NAME_COLUMN, _WORKSHEET_COLUMN_WIDTH);
            _summaryWorksheet.setWidth(_SUMMARY_WORKSHEET_PRODUCT_COUNT_COLUMN, _WORKSHEET_COLUMN_WIDTH);
            _summaryWorksheet.setWidth(_SUMMARY_WORKSHEET_PRODUCT_PRICE_COLUMN, _WORKSHEET_COLUMN_WIDTH);
        }

        private bool _open(string directory) {
            lock (_THREAD_LOCK) {
                if (isOpened)
                    return false;

                var now = DateTime.Now;
                _year = now.Year;
                _month = Utils.getMonthName(now);
                _workbookDirectory = directory;

                Directory.CreateDirectory(_yearlyDirectory);

                _workbookSettings = new ProductWorkbookSettings(_yearlyDirectory);

                var monthlyWorkbookPath = Path.Combine(_yearlyDirectory, _month + ".xlsx");
                // TODO: check if monthlyWorkbook is already open
                //if (ExcelUtils.isWorkbookOpen(monthlyWorkbookPath)) {
                //    Log.e(LOG_TAG, monthlyWorkbookPath + " is already open! Now closing it.");
                //    ExcelUtils.closeApplication(monthlyWorkbookPath);
                //}
                _monthlyWorkbook = new ExcelWorkbook(monthlyWorkbookPath);
                var isNewWorkbook = false;
                if (null == _monthlyWorkbookInfo) {
                    isNewWorkbook = true;
                    _monthlyWorkbookInfo = new ProductWorkbookInfo(_monthlyWorkbook);
                }

                //var summaryWorkbookPath = Path.Combine(_yearlyDirectory, _SUMMARY_NAME + ".xlsx");
                // TODO: check if summaryWorkbookPath is already open
                //_summaryWorkbook = new ExcelWorkbook(summaryWorkbookPath);

                var dailyWorksheetName = now.DayOfWeek.ToString() + ' ' + Utils.getShortDateString(now, ".", true);
                var isNewWorksheet = false;
                if (isNewWorkbook) {
                    isNewWorksheet = true;
                    _dailyWorksheet = _monthlyWorkbook.activeWorksheet;
                    _dailyWorksheet.name = dailyWorksheetName;

                    // add a Summary worksheet if isNewWorkbook
                    _summaryWorksheet = _monthlyWorkbook.addWorksheet(_SUMMARY_NAME);
                } else {
                    _dailyWorksheet = _monthlyWorkbook.getWorksheet(dailyWorksheetName);
                    _summaryWorksheet = _monthlyWorkbook.getWorksheet(_SUMMARY_NAME);

                    if (null == _dailyWorksheet) {
                        isNewWorksheet = true;
                        _dailyWorksheet = _monthlyWorkbook.addWorksheet(dailyWorksheetName);
                    }
                }

                Log.d(_LOG_TAG,
                      "currentYear: " + _year,
                      "currentYearDirectory: " + _yearlyDirectory,
                      "currentMonthWorkbook: " + Path.Combine(_yearlyDirectory, _month + ".xlsx"),
                      "currentDayWorksheet: " + now.DayOfWeek.ToString() + ' ' + Utils.getShortDateString(now, ".", true)
                      );


                var format = "#,##0.00" + CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;
                _INFO_SUMMARY_PRODUCT_PRICE.format = format;
                _INFO_PRODUCT_PRICE.format = format;

                if (isNewWorkbook)
                    _initializeNewMonthlyWorkbook();
                if (isNewWorksheet)
                    _initializeNewDailyWorksheet();

                _isOpened = true;

                return true;
            }
        }

        private void _add(in Product product, in DateTime timepoint) {
            if (!isOpened)
                return;

            lock (_THREAD_LOCK) {
                // Added new product 
                if (!_dailyWorksheetInfoForAddedProducts.productCount.ContainsKey(product))
                    _dailyWorksheetInfoForAddedProducts.productCount.Add(product, 0);
                _dailyWorksheetInfoForAddedProducts.productCount[product]++;

                _dailyWorksheet.cells[_dailyWorksheetInfoForAddedProducts.currentRow, _INFO_TIME_POINT.index] = Utils.getShortTimeString(timepoint);
                _dailyWorksheet.cells[_dailyWorksheetInfoForAddedProducts.currentRow, _INFO_PRODUCT_NAME.index] = product.name;
                _dailyWorksheet.cells[_dailyWorksheetInfoForAddedProducts.currentRow, _INFO_PRODUCT_PRICE.index] = $"{product.priceInCent / 100.0}";

                _dailyWorksheetInfoForAddedProducts.currentRow++;
            }

            _save();
        }

        private void _save() {
            if (!isOpened)
                return;

            lock (_THREAD_LOCK) {
                _monthlyWorkbook.save();
                _workbookSettings.save();
            }
        }

        private void _close(bool save = true) {
            if (!isOpened)
                return;

            lock (_THREAD_LOCK) {
                // TODO: implement _summaryWorkbook
                //_updateSummaryWorkbook();

                _updateSummaryWorksheet();
                _updateProductSummaryColumns(_dailyWorksheet, _dailyWorksheetInfoForAddedProducts.productCount);

                if (save)
                    _workbookSettings?.save();

                _monthlyWorkbook?.close(save);

                // TODO: implement _summaryWorkbook
                //_summaryWorkbook?.close(save);

                _isOpened = false;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// If isOpened is true, the close has to be called before
        /// you can open another directory.
        /// </summary>
        /// <param name="directory">The directory that contains all product workbooks.</param>
        /// <returns>Whether directory was successfully opened or not.</returns>
        public static bool open(string directory) => instance._open(directory);

        public static void add(in Product product, in DateTime timepoint)
            => instance._add(product, timepoint);

        public static void save() => instance._save();

        public static void close(bool save = true) => instance._close(save);

        #endregion

        #region Class Definition ProductWorkbookSettings

        private class ProductWorkbookSettings {

            #region Private Declarations

            private const string _LOG_TAG = nameof(ProductWorkbookSettings);

            private const string _FILE_NAME = ".productWorkbookSettings";

            private const string _FILE_EXTENSION = ".json";

            private string _directory;

            private string _filePath {
                get => Path.Combine(_directory, _FILE_NAME + _FILE_EXTENSION);
            }

            #endregion

            #region Public Declarations

            public ProductWorkbookInfoDictionary workbookInfos {
                get; private set;
            }

            #endregion

            public ProductWorkbookSettings(string currentYearDirectory) {
                _directory = currentYearDirectory;

                if (!File.Exists(_filePath)) {
                    File.Create(_filePath);
                    File.SetAttributes(_filePath, File.GetAttributes(_filePath) | FileAttributes.Hidden);

                    Log.d(_LOG_TAG, "Creating hidden file " + _filePath);

                    workbookInfos = new ProductWorkbookInfoDictionary();
                } else {
                    Log.d(_LOG_TAG, "Reading from " + _filePath);

                    try {
                        string rawJson = File.ReadAllText(_filePath);
                        workbookInfos = JsonConvert.DeserializeObject<ProductWorkbookInfoDictionary>(rawJson);

                        if (null == workbookInfos)
                            workbookInfos = new ProductWorkbookInfoDictionary();

                    } catch (Exception e) {
                        workbookInfos = new ProductWorkbookInfoDictionary();
                        Log.e(_LOG_TAG, e.Message);
                    }
                }
            }

            #region Public Methods

            public void save() {
                string rawJson = JsonConvert.SerializeObject(workbookInfos);

                var info = new FileInfo(_filePath);
                info.Attributes &= ~FileAttributes.Hidden;

                var writer = info.CreateText();
                writer.Write(rawJson);
                writer.Close();

                info.Attributes |= FileAttributes.Hidden;
            }

            public void addInfo(in ProductWorkbookInfo info) => workbookInfos.add(info.workbookPath, info);

            public ProductWorkbookInfo getInfo(string workbookPath) => workbookInfos[workbookPath];

            public ProductWorkbookInfo getInfo(in ExcelWorkbook workbook) => workbookInfos[workbook.filepath];

            public void updateInfo(in ProductWorkbookInfo info) => workbookInfos[info.workbookPath] = info;

            public bool containsInfo(in ProductWorkbookInfo info) => workbookInfos.containsValue(info);

            public bool containsInfoFor(string workbookPath) => workbookInfos.containsKey(workbookPath);

            public bool containsInfoFor(in ExcelWorkbook workbook) => workbookInfos.containsKey(workbook.filepath);

            #endregion
        }

        #endregion
    }
}
