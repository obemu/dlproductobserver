﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DLProductObserver.Backend.Model;

namespace DLProductObserver.Backend.Excel {
    public class ProductWorkbooksExporter {
        #region Private Declarations

        private const string _LOG_TAG = nameof(ProductWorkbooksExporter);

        private const string _DEFAULT_INITIAL_SHEET_NAME = "DEFAULT_SHEET";

        private static readonly string _CURRENCY_COLUMN_FORMAT =
            "#,##0.00" + CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;

        #endregion

        #region Public Declarations

        /// <summary>
        /// The directory to which all the spreadsheets outputted to.
        /// The directory gets created, if it does not exist already.
        /// </summary>
        public readonly string outputDirectoryPath;

        public readonly SheetFormat sheetFormat;

        // TODO: Add support for SheetFormat.OpenSpreadSheet
        /// <summary>
        /// The fileextension of the spreadsheet documents.
        /// </summary>
        public string fileExtension =>
            (SheetFormat.ExcelSpreadSheet == sheetFormat)
            ? ".xlsx"
            : "";

        #endregion

        public ProductWorkbooksExporter(
            string outputDirectory,
            SheetFormat sheetFormat
            ) {
            this.outputDirectoryPath = outputDirectory;
            this.sheetFormat = sheetFormat;
        }

        #region Private Methods

        private void _formatPriceColumn(ref ExcelWorksheet worksheet, int column)
            => worksheet.formatColumn(column, _CURRENCY_COLUMN_FORMAT);

        /// <param name="columnOffset">
        /// How many columns should be left untouched on the left side.
        /// </param>
        private void _addProductSummary(
            ref ExcelWorksheet worksheet,
            List<ProductEvent> productEvents,
            int columnOffset = 0
            ) {
            var nameColumn = 1 + columnOffset;
            var amountColumn = 2 + columnOffset;
            var priceColumn = 3 + columnOffset;

            var products = (from item in productEvents
                            select item.product)
                         .Distinct();

            // TODO: Add variables for titles for each column
            var row = 1;
            worksheet.cells[row, nameColumn] = "Produkt";
            worksheet.cells[row, amountColumn] = "Anzahl";
            worksheet.cells[row, priceColumn] = "Preis";

            var amountColumnCellString = ColumnInfo.columnIndexToCellString(amountColumn);

            foreach (var product in products) {
                row++;

                var amount = (from item in productEvents
                              where item.product == product
                              select item).Count();

                worksheet.cells[row, nameColumn] = product.name;
                worksheet.cells[row, amountColumn] = amount;
                worksheet.cells[row, priceColumn] = $"=({amountColumnCellString}{row} * {product.priceInCent}) / 100";
            }

            _formatPriceColumn(ref worksheet, priceColumn);
        }

        private ExcelWorksheet _getExcelWorksheet(ref ExcelWorkbook workbook, string sheetName) {
            ExcelWorksheet worksheet;

            if (workbook.worksheetNames.Contains(_DEFAULT_INITIAL_SHEET_NAME)) {
                worksheet = workbook.getWorksheet(_DEFAULT_INITIAL_SHEET_NAME);
                worksheet.name = sheetName;
                return worksheet;
            }

            if (workbook.worksheetNames.Contains(sheetName)) {
                worksheet = workbook.getWorksheet(sheetName);
                return worksheet;
            }

            worksheet = workbook.addWorksheet(sheetName);

            return worksheet;
        }

        /// <summary>
        /// Sorts the List by the recorded timepoint of the
        /// ProductEvents, in an ascending order.
        /// </summary>
        private void _sortProductEventList(ref List<ProductEvent> productEvents)
            => productEvents.Sort((a, b) => a.timepoint.CompareTo(b.timepoint));

        private string _getDailySheetName(in DateTime dateTime)
            => dateTime.DayOfWeek.ToString() + ' '
            + Utils.getShortDateString(dateTime, ".", true);

        private string _getDocumentFileName(string name)
            => $"{name}{fileExtension}";

        private bool _createDirectoriesAndDocuments(List<ProductEvent> data) {
            try {
                if (Directory.Exists(outputDirectoryPath))
                    Directory.Delete(outputDirectoryPath, true);

                Directory.CreateDirectory(outputDirectoryPath);
            } catch (Exception e) {
                Log.e(_LOG_TAG, e.Message);
                return false;
            }

            // Create all necessary directories for the years
            var years = (from item in data
                         select item.timepoint.Year)
                        .Distinct();

            foreach (var year in years) {
                var yearDirectoryPath = Path.Combine(outputDirectoryPath, year.ToString());

                try {
                    Directory.CreateDirectory(yearDirectoryPath);
                } catch (Exception e) {
                    Log.e(_LOG_TAG,
                        $"When creating directory \"{yearDirectoryPath}\"",
                        e.Message);
                    return false;
                }

                // Create all spreadsheets in this yearDirectory
                var months = (from item in data
                              where item.timepoint.Year == year
                              select Utils.getMonthName(item.timepoint.Month))
                             .Distinct();

                foreach (var month in months) {
                    var monthSpreadSheetPath = Path.Combine(
                        yearDirectoryPath,
                        _getDocumentFileName(month)
                        );

                    if (!ExcelUtils.createWorkbook(monthSpreadSheetPath))
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="year">
        /// The year in which the ProductEvents happened.
        /// </param>
        /// <param name="productEvents">
        /// A list containing all events that happened in this year.
        /// </param>
        private bool _fillDocumentsForYear(int year, IEnumerable<ProductEvent> productEvents) {
            var yearDirectoryPath = Path.Combine(outputDirectoryPath, year.ToString());

            var months = (from item in productEvents
                          select item.timepoint.Month)
                        .Distinct();

            foreach (var month in months) {
                var eventsOfMonth = from item in productEvents
                                    where item.timepoint.Month == month
                                    select item;

                if (!_fillDocumentForMonth(
                     Path.Combine(
                         yearDirectoryPath,
                         _getDocumentFileName(Utils.getMonthName(month))
                         ),
                     eventsOfMonth
                     ))
                    return false;
            }

            return true;
        }


        // TODO: Add support for SheetFormat.OpenSpreadSheet
        private bool _fillDocumentForMonth(string filePath, IEnumerable<ProductEvent> productEvents) {
            if (0 == productEvents.Count())
                return true;

            ExcelWorkbook workbook = new ExcelWorkbook();

            if (!workbook.open(filePath))
                return false;

            workbook.activeWorksheet.name = _DEFAULT_INITIAL_SHEET_NAME;

            var days = (from item in productEvents
                        select item.timepoint.Day)
                       .Distinct();

            foreach (var day in days) {
                var eventsOfDay = from item in productEvents
                                  where item.timepoint.Day == day
                                  select item;

                _fillSheetForDay(ref workbook, eventsOfDay.ToList());
            }

            _fillSummarySheet(ref workbook, productEvents.ToList());
            workbook.close();

            return true;
        }

        // TODO: Add support for SheetFormat.OpenSpreadSheet
        private void _fillSheetForDay(
            ref ExcelWorkbook workbook,
            List<ProductEvent> productEvents
            ) {
            if (0 == productEvents.Count())
                return;

            // Get the ExcelWorksheet for the day
            var sheetName = _getDailySheetName(productEvents[0].timepoint);
            ExcelWorksheet worksheet = _getExcelWorksheet(ref workbook, sheetName);

            _sortProductEventList(ref productEvents);

            var timepointColumn = 1;
            var nameColumn = 2;
            var priceColumn = 3;

            // TODO: Add variables for titles for each column
            var row = 1;
            worksheet.cells[row, timepointColumn] = "Zeitpunkt";
            worksheet.cells[row, nameColumn] = "Produkt";
            worksheet.cells[row, priceColumn] = "Preis";

            // Add product timeline
            foreach (var pEvent in productEvents) {
                row++;

                worksheet.cells[row, timepointColumn] = Utils.getShortTimeString(pEvent.timepoint);
                worksheet.cells[row, nameColumn] = pEvent.product.name;
                worksheet.cells[row, priceColumn] = $"={pEvent.product.priceInCent} / 100";
            }

            _formatPriceColumn(ref worksheet, priceColumn);

            // Add product summary
            _addProductSummary(ref worksheet, productEvents, priceColumn + 2);
        }

        private void _fillSummarySheet(
            ref ExcelWorkbook workbook,
            List<ProductEvent> productEvents
            ) {
            if (0 == productEvents.Count())
                return;

            // TODO: Add variable or constant for sheetName
            var sheetName = "Summary";
            ExcelWorksheet worksheet = _getExcelWorksheet(ref workbook, sheetName);

            _addProductSummary(ref worksheet, productEvents);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Export a list of ProductEvents to spreadsheets.
        /// <br></br>
        /// The output is placed in the outputDirectory,
        /// if the outputDirectory already exists, then it
        /// gets recursively deleted before it is used.
        /// </summary>
        /// <param name="data">
        /// A list containing all the ProductEvents that should be
        /// exported to spreadsheets.
        /// </param>
        /// <returns>
        /// True if the data was successfully exported, else false.
        /// </returns>
        public bool export(List<ProductEvent> data) {
            if (!_createDirectoriesAndDocuments(data))
                return false;

            var years = (from item in data
                         select item.timepoint.Year)
                      .Distinct();

            foreach (var year in years) {
                var eventsOfYear = from item in data
                                   where item.timepoint.Year == year
                                   select item;

                if (!_fillDocumentsForYear(year, eventsOfYear))
                    return false;
            }

            return true;
        }

        #endregion

        public enum SheetFormat {
            ExcelSpreadSheet,

            // TODO: Add support for OpenSpreadSheet format
            // OpenSpreadSheet
        }
    }
}
