﻿using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;

namespace DLProductObserver.Backend.Database {
    public class DataReader {

        #region Private Declarations

        private readonly SqlDataReader _reader;

        #endregion

        public DataReader(ref SqlDataReader reader) {
            _reader = reader;
        }

        #region Public Methods

        public object this[string name] => _reader[name];

        public object this[int i] => _reader[i];

        public int VisibleFieldCount => _reader.VisibleFieldCount;

        public int FieldCount => _reader.FieldCount;

        public int Depth => _reader.Depth;

        public bool GetBoolean(int i) => _reader.GetBoolean(i);

        public byte GetByte(int i) => _reader.GetByte(i);

        public long GetBytes(int i, long dataIndex, byte[] buffer, int bufferIndex, int length)
            => _reader.GetBytes(i, dataIndex, buffer, bufferIndex, length);

        public char GetChar(int i) => _reader.GetChar(i);

        public long GetChars(int i, long dataIndex, char[] buffer, int bufferIndex, int length)
            => _reader.GetChars(i, dataIndex, buffer, bufferIndex, length);

        public string GetDataTypeName(int i) => _reader.GetDataTypeName(i);

        public DateTime GetDateTime(int i) => _reader.GetDateTime(i);

        public DateTimeOffset GetDateTimeOffset(int i) => _reader.GetDateTimeOffset(i);

        public decimal GetDecimal(int i) => _reader.GetDecimal(i);

        public double GetDouble(int i) => _reader.GetDouble(i);

        public Type GetFieldType(int i) => _reader.GetFieldType(i);

        public T GetFieldValue<T>(int i) => _reader.GetFieldValue<T>(i);

        public float GetFloat(int i) => _reader.GetFloat(i);

        public Guid GetGuid(int i) => _reader.GetGuid(i);

        public short GetInt16(int i) => _reader.GetInt16(i);

        public int GetInt32(int i) => _reader.GetInt32(i);

        public long GetInt64(int i) => _reader.GetInt64(i);

        public string GetName(int i) => _reader.GetName(i);

        public SqlBinary GetSqlBinary(int i) => _reader.GetSqlBinary(i);

        public SqlBoolean GetSqlBoolean(int i) => _reader.GetSqlBoolean(i);

        public SqlByte GetSqlByte(int i) => _reader.GetSqlByte(i);

        public SqlBytes GetSqlBytes(int i) => _reader.GetSqlBytes(i);

        public SqlChars GetSqlChars(int i) => _reader.GetSqlChars(i);

        public SqlDateTime GetSqlDateTime(int i) => _reader.GetSqlDateTime(i);

        public SqlDecimal GetSqlDecimal(int i) => _reader.GetSqlDecimal(i);

        public SqlDouble GetSqlDouble(int i) => _reader.GetSqlDouble(i);

        public SqlGuid GetSqlGuid(int i) => _reader.GetSqlGuid(i);

        public SqlInt16 GetSqlInt16(int i) => _reader.GetSqlInt16(i);

        public SqlInt32 GetSqlInt32(int i) => _reader.GetSqlInt32(i);

        public SqlInt64 GetSqlInt64(int i) => _reader.GetSqlInt64(i);

        public SqlMoney GetSqlMoney(int i) => _reader.GetSqlMoney(i);

        public SqlSingle GetSqlSingle(int i) => _reader.GetSqlSingle(i);

        public SqlString GetSqlString(int i) => _reader.GetSqlString(i);

        public object GetSqlValue(int i) => _reader.GetSqlValue(i);

        public int GetSqlValues(object[] values) => _reader.GetSqlValues(values);

        public SqlXml GetSqlXml(int i) => _reader.GetSqlXml(i);

        public Stream GetStream(int i) => _reader.GetStream(i);

        public string GetString(int i) => _reader.GetString(i);

        public TextReader GetTextReader(int i) => _reader.GetTextReader(i);

        public TimeSpan GetTimeSpan(int i) => _reader.GetTimeSpan(i);

        public object GetValue(int i) => _reader.GetValue(i);

        public int GetValues(object[] values) => _reader.GetValues(values);

        public bool IsDBNull(int i) => _reader.IsDBNull(i);

        #endregion
    }
}
