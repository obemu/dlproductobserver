﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DLProductObserver.Backend.Model;

namespace DLProductObserver.Backend.Database {
    public static class ScannedProductsDB {

        #region Private Declarations

        private static readonly string _CONNECTION_STRING =
            Properties.Settings.Default.ScannedProductsDatabaseConnectionString;

        private static DBUtils _dbUtils;

        #endregion

        #region Public Methods

        public static void initialize() {
            _dbUtils = new DBUtils(_CONNECTION_STRING);
        }

        public static void add(ProductEventArgs args) {
            SqlCommand command = new SqlCommand();

            command.CommandText =
                "INSERT INTO Table_ScannedProducts"
                + " ([ScanDateTime], [ProductName], [ProductPriceInCent], [ProductBarcode])"
                + " VALUES(@date, @name, @price, @barcode)";

            command.Parameters.AddWithValue("@date", args.productEvent.timepoint);
            command.Parameters.AddWithValue("@name", args.productEvent.product.name);
            command.Parameters.AddWithValue("@price", args.productEvent.product.priceInCent);
            command.Parameters.AddWithValue("@barcode", args.productEvent.product.barcode);

            _dbUtils.executeCommand(command);
        }

        public static List<ProductEvent> getData() {
            var list = new List<ProductEvent>();

            _dbUtils.readEntireTable("Table_ScannedProducts", (reader) => {
                list.Add(
                    new ProductEvent(
                        new Product(
                                reader.GetString(1),
                                reader.GetString(3),
                                reader.GetInt32(2)
                            ),
                        reader.GetDateTime(0)
                        )
                    );
            });

            return list;
        }

        #endregion
    }
}
