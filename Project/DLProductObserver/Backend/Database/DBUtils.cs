﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;

namespace DLProductObserver.Backend.Database {
    public class DBUtils {

        #region Private Declarations

        private const string _LOG_TAG = nameof(DBUtils);

        private string _connectionString;

        #endregion

        public DBUtils(string connectionString) {
            this._connectionString = connectionString;
        }

        #region Private Methods

        public bool _establishConnection(out SqlConnection connection) {
            connection = new SqlConnection(_connectionString);

            try {
                if (ConnectionState.Open != connection.State)
                    connection.Open();
            } catch (Exception e) {
                Log.e(_LOG_TAG, e.Message);
                connection = null;
                return false;
            }

            return true;
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///  Executes a Transact-SQL statement against the connection
        ///  and returns the number of rows affected.
        ///  <br></br>
        ///  The connection is automatically set.
        /// </summary>
        /// <returns>
        /// The number of rows affected.
        /// <br></br>
        /// If an exception happened, or isDatabaseOpen is false, then
        /// -1 gets returned.
        /// </returns>
        public int executeCommand(SqlCommand command) {
            SqlConnection connection;
            if (!_establishConnection(out connection))
                return -1;

            command.Connection = connection;

            int rows = -1;

            try {
                rows = command?.ExecuteNonQuery() ?? -1;
            } catch (Exception e) {
                Log.e(_LOG_TAG, e.Message);
            }

            connection.Close();

            return rows;
        }

        public void readEntireTable(string table, Action<DataReader> forEachRow) {
            SqlConnection connection;
            if (!_establishConnection(out connection))
                return;

            var reader = new SqlCommand("SELECT * FROM " + table, connection).ExecuteReader();
            var dataReader = new DataReader(ref reader);

            while (reader.Read())
                forEachRow.Invoke(dataReader);

            reader.Close();
            connection.Close();
        }

        /// <returns>
        /// The path to the local database file, or null if
        /// no connection could be established.
        /// </returns>
        public string getDatabaseName() {
            SqlConnection connection;
            if (!_establishConnection(out connection))
                return null;

            return connection.Database;
        }

        // TODO: Implement backupDatabase
        public void backupDatabase(string filePath) {
            throw new NotImplementedException();

            Backup fullBackup = new Backup();

            fullBackup.Action = BackupActionType.Database;
        }

        #endregion
    }
}
