﻿using System;
using Newtonsoft.Json;

using DLProductObserver.Backend.Model;
using System.Windows.Forms;

namespace DLProductObserver.Backend {
    public static class Settings {

        #region Private Declarations

        private const string _LOG_TAG = nameof(Settings);

        private static readonly object _THREAD_LOCK = new object();

        private static readonly Properties.Settings _DEFAULT_SETTINGS = Properties.Settings.Default;

        #endregion

        #region Public Declarations

        public static bool isSaving {
            get; private set;
        }

        public static readonly ProductList products;

        public static readonly DeviceEntityList deviceEntities;

        public static int productListViewSortingColumn {
            get => _DEFAULT_SETTINGS.productListViewSortingColumn;
            set => _DEFAULT_SETTINGS.productListViewSortingColumn = value;
        }

        public static int deviceListViewSortingColumn {
            get => _DEFAULT_SETTINGS.deviceListViewSortingColumn;
            set => _DEFAULT_SETTINGS.deviceListViewSortingColumn = value;
        }

        public static SortOrder listViewSortingOrder {
            get => _DEFAULT_SETTINGS.listViewSortingOrder;
            set => _DEFAULT_SETTINGS.listViewSortingOrder = value;
        }

        public static string outputDirectoryPath {
            get => _DEFAULT_SETTINGS.outputDirectoryPath;
            set => _DEFAULT_SETTINGS.outputDirectoryPath = value;
        }

        #endregion

        static Settings() {
            _initializeParameter(
                    _DEFAULT_SETTINGS.serializedProducts,
                    out products,
                    new ProductList(),
                     (e) => Log.e(_LOG_TAG, "While deserializing products", e.Message)
                );

            _initializeParameter(
                   _DEFAULT_SETTINGS.serializedDeviceEntities,
                   out deviceEntities,
                   new DeviceEntityList(),
                    (e) => Log.e(_LOG_TAG, "While deserializing deviceEntities", e.Message)
               );
        }

        #region Private Methods

        /// <summary></summary>
        /// <param name="convert">
        /// If not null than this callback is used for converting
        /// the rawJson to T instead of JsonConvert.DeserializeObject<T>(rawJson).
        /// </param>
        /// <returns>True if param successfully initialized, else false.</returns>
        private static bool _initializeParameter<T>(
                string rawJson,
                out T param,
                T defaultValue,
                Action<Exception> onError,
                Func<string, T> convert = null
            ) {

            if (Properties.Settings.NULL_VALUE == rawJson && null == convert) {
                param = defaultValue;
                return true;
            }

            try {
                if (null == convert) {
                    param = JsonConvert.DeserializeObject<T>(rawJson);
                } else {
                    param = convert.Invoke(rawJson);
                }

                if (null == param)
                    param = defaultValue;

            } catch (Exception e) {
                param = defaultValue;
                onError?.Invoke(e);
                return false;
            }

            return true;
        }

        #endregion

        #region Public Methods

        public static void save() {
            lock (_THREAD_LOCK) {
                isSaving = true;

                _DEFAULT_SETTINGS.serializedProducts =
                    (0 == products.Count)
                    ? Properties.Settings.NULL_VALUE
                    : JsonConvert.SerializeObject(products);

                _DEFAULT_SETTINGS.serializedDeviceEntities =
                    (0 == deviceEntities.Count)
                    ? Properties.Settings.NULL_VALUE
                    : JsonConvert.SerializeObject(deviceEntities);

                _DEFAULT_SETTINGS.Save();

                isSaving = false;
            }
        }

        #endregion
    }
}
