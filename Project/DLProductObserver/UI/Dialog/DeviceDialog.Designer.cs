﻿
namespace DLProductObserver.UI.Dialog {
    partial class DeviceDialog {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.parameterInputTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.productIdTextBox = new System.Windows.Forms.TextBox();
            this.deviceNameTextBox = new System.Windows.Forms.TextBox();
            this.vendorIdTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.parameterInputTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.parameterInputTableLayoutPanel, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(526, 305);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.okButton);
            this.flowLayoutPanel1.Controls.Add(this.cancelButton);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 262);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(520, 40);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // okButton
            // 
            this.okButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.okButton.Location = new System.Drawing.Point(432, 3);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(85, 30);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cancelButton.Location = new System.Drawing.Point(341, 3);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(85, 30);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // parameterInputTableLayoutPanel
            // 
            this.parameterInputTableLayoutPanel.ColumnCount = 1;
            this.parameterInputTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.parameterInputTableLayoutPanel.Controls.Add(this.productIdTextBox, 0, 2);
            this.parameterInputTableLayoutPanel.Controls.Add(this.deviceNameTextBox, 0, 0);
            this.parameterInputTableLayoutPanel.Controls.Add(this.vendorIdTextBox, 0, 1);
            this.parameterInputTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parameterInputTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.parameterInputTableLayoutPanel.Name = "parameterInputTableLayoutPanel";
            this.parameterInputTableLayoutPanel.RowCount = 3;
            this.parameterInputTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42F));
            this.parameterInputTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.parameterInputTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42F));
            this.parameterInputTableLayoutPanel.Size = new System.Drawing.Size(520, 253);
            this.parameterInputTableLayoutPanel.TabIndex = 3;
            // 
            // productIdTextBox
            // 
            this.productIdTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.productIdTextBox.Location = new System.Drawing.Point(147, 149);
            this.productIdTextBox.Name = "productIdTextBox";
            this.productIdTextBox.Size = new System.Drawing.Size(225, 22);
            this.productIdTextBox.TabIndex = 2;
            this.productIdTextBox.TextChanged += new System.EventHandler(this.productIdTextBox_TextChanged);
            this.productIdTextBox.Enter += new System.EventHandler(this.productIdTextBox_Enter);
            this.productIdTextBox.Leave += new System.EventHandler(this.productIdTextBox_Leave);
            this.productIdTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.productIdTextBox_Validating);
            // 
            // deviceNameTextBox
            // 
            this.deviceNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.deviceNameTextBox.Location = new System.Drawing.Point(147, 81);
            this.deviceNameTextBox.Name = "deviceNameTextBox";
            this.deviceNameTextBox.Size = new System.Drawing.Size(225, 22);
            this.deviceNameTextBox.TabIndex = 0;
            this.deviceNameTextBox.TextChanged += new System.EventHandler(this.deviceNameTextBox_TextChanged);
            this.deviceNameTextBox.Enter += new System.EventHandler(this.deviceNameTextBox_Enter);
            this.deviceNameTextBox.Leave += new System.EventHandler(this.deviceNameTextBox_Leave);
            // 
            // vendorIdTextBox
            // 
            this.vendorIdTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.vendorIdTextBox.Location = new System.Drawing.Point(147, 115);
            this.vendorIdTextBox.Name = "vendorIdTextBox";
            this.vendorIdTextBox.Size = new System.Drawing.Size(225, 22);
            this.vendorIdTextBox.TabIndex = 1;
            this.vendorIdTextBox.TextChanged += new System.EventHandler(this.vendorIdTextBox_TextChanged);
            this.vendorIdTextBox.Enter += new System.EventHandler(this.vendorIdTextBox_Enter);
            this.vendorIdTextBox.Leave += new System.EventHandler(this.vendorIdTextBox_Leave);
            this.vendorIdTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.vendorIdTextBox_Validating);
            // 
            // DeviceDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 305);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.Name = "DeviceDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.parameterInputTableLayoutPanel.ResumeLayout(false);
            this.parameterInputTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TableLayoutPanel parameterInputTableLayoutPanel;
        private System.Windows.Forms.TextBox deviceNameTextBox;
        private System.Windows.Forms.TextBox vendorIdTextBox;
        private System.Windows.Forms.TextBox productIdTextBox;
    }
}