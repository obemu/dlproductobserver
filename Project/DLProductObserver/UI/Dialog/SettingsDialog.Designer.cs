﻿
namespace DLProductObserver.UI.Dialog {
    partial class SettingsDialog {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsDialog));
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.addDeviceButton = new DLProductObserverControls.NoFocusCueButton();
            this.editDeviceButton = new DLProductObserverControls.NoFocusCueButton();
            this.removeDeviceButton = new DLProductObserverControls.NoFocusCueButton();
            this.deviceEntityListView = new System.Windows.Forms.ListView();
            this.deviceNameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.vendorIdHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.productIdHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.outputDirectoryLabel = new System.Windows.Forms.Label();
            this.chooseDirectoryButton = new DLProductObserverControls.NoFocusCueButton();
            this.outputDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelMain.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.AutoScroll = true;
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.flowLayoutPanel1, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.deviceEntityListView, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanel1, 0, 4);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 5;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 62F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(923, 467);
            this.tableLayoutPanelMain.TabIndex = 5;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.addDeviceButton);
            this.flowLayoutPanel1.Controls.Add(this.editDeviceButton);
            this.flowLayoutPanel1.Controls.Add(this.removeDeviceButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 310);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flowLayoutPanel1.Size = new System.Drawing.Size(917, 40);
            this.flowLayoutPanel1.TabIndex = 10;
            // 
            // addDeviceButton
            // 
            this.addDeviceButton.Location = new System.Drawing.Point(829, 3);
            this.addDeviceButton.Name = "addDeviceButton";
            this.addDeviceButton.Size = new System.Drawing.Size(85, 30);
            this.addDeviceButton.TabIndex = 0;
            this.addDeviceButton.Text = "Add";
            this.addDeviceButton.UseVisualStyleBackColor = true;
            this.addDeviceButton.Click += new System.EventHandler(this.addDeviceButton_Click);
            // 
            // editDeviceButton
            // 
            this.editDeviceButton.Location = new System.Drawing.Point(738, 3);
            this.editDeviceButton.Name = "editDeviceButton";
            this.editDeviceButton.Size = new System.Drawing.Size(85, 30);
            this.editDeviceButton.TabIndex = 1;
            this.editDeviceButton.Text = "Edit";
            this.editDeviceButton.UseVisualStyleBackColor = true;
            this.editDeviceButton.Click += new System.EventHandler(this.editDeviceButton_Click);
            // 
            // removeDeviceButton
            // 
            this.removeDeviceButton.Location = new System.Drawing.Point(647, 3);
            this.removeDeviceButton.Name = "removeDeviceButton";
            this.removeDeviceButton.Size = new System.Drawing.Size(85, 30);
            this.removeDeviceButton.TabIndex = 2;
            this.removeDeviceButton.Text = "Remove";
            this.removeDeviceButton.UseVisualStyleBackColor = true;
            this.removeDeviceButton.Click += new System.EventHandler(this.removeDeviceButton_Click);
            // 
            // deviceEntityListView
            // 
            this.deviceEntityListView.CheckBoxes = true;
            this.deviceEntityListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.deviceNameHeader,
            this.vendorIdHeader,
            this.productIdHeader});
            this.deviceEntityListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deviceEntityListView.FullRowSelect = true;
            this.deviceEntityListView.GridLines = true;
            this.deviceEntityListView.HideSelection = false;
            this.deviceEntityListView.Location = new System.Drawing.Point(3, 3);
            this.deviceEntityListView.MultiSelect = false;
            this.deviceEntityListView.Name = "deviceEntityListView";
            this.deviceEntityListView.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.deviceEntityListView.Size = new System.Drawing.Size(917, 283);
            this.deviceEntityListView.TabIndex = 8;
            this.deviceEntityListView.TabStop = false;
            this.deviceEntityListView.UseCompatibleStateImageBehavior = false;
            this.deviceEntityListView.View = System.Windows.Forms.View.Details;
            this.deviceEntityListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.deviceEntityListView_ColumnClick);
            this.deviceEntityListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.deviceEntityListView_ItemChecked);
            // 
            // deviceNameHeader
            // 
            this.deviceNameHeader.Text = "Device Name";
            this.deviceNameHeader.Width = 130;
            // 
            // vendorIdHeader
            // 
            this.vendorIdHeader.Text = "Vendor ID (VID)";
            this.vendorIdHeader.Width = 130;
            // 
            // productIdHeader
            // 
            this.productIdHeader.Text = "Product ID (PID)";
            this.productIdHeader.Width = 130;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.59106F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.40894F));
            this.tableLayoutPanel1.Controls.Add(this.outputDirectoryLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.chooseDirectoryButton, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.outputDirectoryTextBox, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 374);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(917, 90);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // outputDirectoryLabel
            // 
            this.outputDirectoryLabel.AutoSize = true;
            this.outputDirectoryLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outputDirectoryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputDirectoryLabel.Location = new System.Drawing.Point(3, 0);
            this.outputDirectoryLabel.Name = "outputDirectoryLabel";
            this.outputDirectoryLabel.Size = new System.Drawing.Size(677, 45);
            this.outputDirectoryLabel.TabIndex = 0;
            this.outputDirectoryLabel.Text = "Path to the directory where the excel workbooks should be stored";
            this.outputDirectoryLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // chooseDirectoryButton
            // 
            this.chooseDirectoryButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chooseDirectoryButton.Location = new System.Drawing.Point(829, 52);
            this.chooseDirectoryButton.Name = "chooseDirectoryButton";
            this.chooseDirectoryButton.Size = new System.Drawing.Size(85, 30);
            this.chooseDirectoryButton.TabIndex = 1;
            this.chooseDirectoryButton.Text = "Choose";
            this.chooseDirectoryButton.UseVisualStyleBackColor = true;
            this.chooseDirectoryButton.Click += new System.EventHandler(this.chooseDirectoryButton_Click);
            // 
            // outputDirectoryTextBox
            // 
            this.outputDirectoryTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.outputDirectoryTextBox.Location = new System.Drawing.Point(3, 56);
            this.outputDirectoryTextBox.Name = "outputDirectoryTextBox";
            this.outputDirectoryTextBox.ReadOnly = true;
            this.outputDirectoryTextBox.Size = new System.Drawing.Size(677, 22);
            this.outputDirectoryTextBox.TabIndex = 2;
            // 
            // SettingsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 467);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SettingsDialog";
            this.Text = "Settings";
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DLProductObserverControls.NoFocusCueButton addDeviceButton;
        private DLProductObserverControls.NoFocusCueButton editDeviceButton;
        private DLProductObserverControls.NoFocusCueButton removeDeviceButton;
        private System.Windows.Forms.ListView deviceEntityListView;
        private System.Windows.Forms.ColumnHeader deviceNameHeader;
        private System.Windows.Forms.ColumnHeader vendorIdHeader;
        private System.Windows.Forms.ColumnHeader productIdHeader;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label outputDirectoryLabel;
        private DLProductObserverControls.NoFocusCueButton chooseDirectoryButton;
        private System.Windows.Forms.TextBox outputDirectoryTextBox;
    }
}