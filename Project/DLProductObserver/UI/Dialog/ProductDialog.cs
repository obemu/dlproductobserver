﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DLProductObserver.Backend;
using DLProductObserver.Backend.Model;

namespace DLProductObserver.UI.Dialog {
    public partial class ProductDialog : Form {

        #region Private Declarations

        private const string _DEFAULT_PLACEHOLDER_NAME = "Name (e.g.: Produkt 1)";
        private const string _DEFAULT_PLACEHOLDER_BARCODE = "Barcode (e.g.: 9734649502)";
        private const string _DEFAULT_PLACEHOLDER_PRICE = "Price (e.g.: 2,30)";

        private readonly string _placeholderName;
        private readonly string _placeholderBarcode;
        private readonly string _placeholderPrice;

        private string _name;
        private string _barcode;
        private string _price;

        private bool _clearPlaceholder;

        #endregion

        #region Public Declarations

        public Product product => new Product(_name, _barcode, _price);

        #endregion

        public ProductDialog(Product product, bool clearPlaceholder = true) {
            InitializeComponent();

            _placeholderName = product?.name ?? _DEFAULT_PLACEHOLDER_NAME;
            _placeholderBarcode = product?.barcode ?? _DEFAULT_PLACEHOLDER_BARCODE;
            _placeholderPrice = product?.price ?? _DEFAULT_PLACEHOLDER_PRICE;

            _name = _placeholderName;
            _barcode = _placeholderBarcode;
            _price = _placeholderPrice;

            _clearPlaceholder = clearPlaceholder;

            nameTextBox.Text = _placeholderName;
            barcodeTextBox.Text = _placeholderBarcode;
            priceTextBox.Text = _placeholderPrice;
        }

        public ProductDialog(bool clearPlaceholder = true)
            : this(null, clearPlaceholder) { }

        #region Private Methods

        #region UI Eventhandlers

        private void cancelButton_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void okButton_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.OK;
            Close();
        }

        /************************ nameTextBox ************************/
        private void nameTextBox_Enter(object sender, EventArgs e) {
            if (_clearPlaceholder && _placeholderName == nameTextBox.Text)
                nameTextBox.Text = "";
        }

        private void nameTextBox_TextChanged(object sender, EventArgs e) {
            _name = nameTextBox.Text;
        }

        private void nameTextBox_Leave(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(nameTextBox.Text.Trim()))
                nameTextBox.Text = _placeholderName;
        }

        /************************ barcodeTextBox ************************/
        private void barcodeTextBox_Enter(object sender, EventArgs e) {
            if (_clearPlaceholder && _placeholderBarcode == barcodeTextBox.Text)
                barcodeTextBox.Text = "";
        }

        private void barcodeTextBox_TextChanged(object sender, EventArgs e) {
            _barcode = barcodeTextBox.Text;
        }

        private void barcodeTextBox_Leave(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(barcodeTextBox.Text.Trim()))
                barcodeTextBox.Text = _placeholderBarcode;
        }

        private void barcodeTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e) {
            if (_DEFAULT_PLACEHOLDER_BARCODE == _barcode) {
                Utils.showErrorMessage(
                    "Invalid barcode",
                    "The barcode can only contain numbers from 0 to 9."
                    );
                e.Cancel = true;
                return;
            }

            if (!Regex.IsMatch(_barcode, @"^[0-9]+$")) {
                Utils.showErrorMessage(
                    "Invalid barcode",
                    "The barcode can only contain numbers from 0 to 9."
                    );
                e.Cancel = true;
                return;
            }

            foreach (var product in Settings.products) {
                if (product.barcode == _barcode) {
                    Utils.showErrorMessage(
                        "Invalid barcode",
                        $"There can only be one product with the barcode '{_barcode}'"
                        + $" and it already belongs to the product '{product.name}'."
                        );
                    e.Cancel = true;
                    return;
                }
            }

            e.Cancel = false;
        }

        /************************ priceTextBox ************************/
        private void priceTextBox_Enter(object sender, EventArgs e) {
            if (_clearPlaceholder && _placeholderPrice == priceTextBox.Text)
                priceTextBox.Text = "";
        }

        private void priceTextBox_TextChanged(object sender, EventArgs e) {
            _price = priceTextBox.Text;
        }

        private void priceTextBox_Leave(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(priceTextBox.Text.Trim()))
                priceTextBox.Text = _placeholderPrice;
        }

        private void priceTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e) {
            if (_DEFAULT_PLACEHOLDER_PRICE == _price) {
                Utils.showErrorMessage(
                        "Invalid price",
                        "The price can only contain numbers from 0 to 9 and a single comma (',')."
                        );
                e.Cancel = true;
                return;
            }

            if (!Regex.IsMatch(_price, @"^[0-9,]+$")) {
                Utils.showErrorMessage(
                        "Invalid price",
                        "The price can only contain numbers from 0 to 9 and a single comma (',')."
                        );
                e.Cancel = true;
                return;
            }

            if (_price.StartsWith(",")) {
                Utils.showErrorMessage(
                     "Invalid price",
                     "The price cannot start with a comma (',')."
                     );
                e.Cancel = true;
                return;
            }

            bool containsComma = false;
            foreach (var character in _price) {
                if (',' == character) {
                    if (containsComma) {
                        Utils.showErrorMessage(
                             "Invalid price",
                             "The price can only contain a single comma (',')."
                         );
                        e.Cancel = true;
                        return;
                    }
                    containsComma = true;
                }
            }

            var split = _price.Split(',');
            if (1 == split.Length) {
                _price = $"{split[0]},00";
            } else {

                if (2 < split[1].Length) {
                    Utils.showErrorMessage(
                            "Invalid price",
                            "The price can only have two decimal points."
                        );
                    e.Cancel = true;
                    return;
                }

                var zeroPadding = "";
                for (int i = 0; i < (-1 * (split[1].Length - 2)); i++)
                    zeroPadding += '0';
                _price = $"{split[0]},{split[1]}{zeroPadding}";
            }
            priceTextBox.Text = _price;

            e.Cancel = false;
        }

        #endregion

        #endregion

        #region Public Methods

        public DialogResult show(string title) {
            Text = title;
            return ShowDialog();
        }

        #endregion
    }
}
