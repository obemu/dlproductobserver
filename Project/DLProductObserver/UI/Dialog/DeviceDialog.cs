﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DLProductObserver.Backend;
using DLProductObserver.Backend.Model;

namespace DLProductObserver.UI.Dialog {
    public partial class DeviceDialog : Form {

        #region Private Declarations

        private const string _DEFAULT_PLACEHOLDER_DEVICE_NAME = "Device Name (e.g.: Gerät 1)";
        private const string _DEFAULT_PLACEHOLDER_VENDOR_ID = "VID (e.g.: 11CA)";
        private const string _DEFAULT_PLACEHOLDER_PRODUCT_ID = "PID (e.g.: 0023)";

        private readonly string _deviceNamePlaceHolder;
        private readonly string _vendorIdPlaceHolder;
        private readonly string _productIdPlaceHolder;

        private bool _clearPlaceholder;

        private string _deviceName;
        private string _vendorId;
        private string _productId;

        #endregion

        #region Public Declarations

        public DeviceEntity deviceEntity
            => new DeviceEntity(_deviceName, _vendorId, _productId);

        #endregion

        public DeviceDialog(
            string deviceName, string vendorId,
            string productId, bool clearPlaceholder = true
            ) {
            InitializeComponent();

            _deviceNamePlaceHolder = deviceName ?? _DEFAULT_PLACEHOLDER_DEVICE_NAME;
            _vendorIdPlaceHolder = vendorId ?? _DEFAULT_PLACEHOLDER_VENDOR_ID;
            _productIdPlaceHolder = productId ?? _DEFAULT_PLACEHOLDER_PRODUCT_ID;

            _clearPlaceholder = clearPlaceholder;

            deviceNameTextBox.Text = _deviceNamePlaceHolder;
            vendorIdTextBox.Text = _vendorIdPlaceHolder;
            productIdTextBox.Text = _productIdPlaceHolder;
        }

        public DeviceDialog(DeviceEntity entity, bool clearPlaceholder = true)
            : this(
                  entity?.simpleName, entity?.vendorId,
                  entity?.productId, clearPlaceholder
                  ) {
        }

        public DeviceDialog() : this(null, null, null) { }

        #region Private Methods

        private void _showInvalidIdErrorMessage(string typeOfId) {
            Utils.showErrorMessage(
                       "Invalid " + typeOfId + " ID",
                       "The id can only contain capital letters from A to Z or numbers from 0 to 9."
                       );
        }

        #region UI Eventhandlers

        private void cancelButton_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void okButton_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.OK;
            Close();
        }

        /************************ deviceNameTextBox ************************/
        private void deviceNameTextBox_Enter(object sender, EventArgs e) {
            if (_clearPlaceholder && _deviceNamePlaceHolder == deviceNameTextBox.Text)
                deviceNameTextBox.Text = "";
        }

        private void deviceNameTextBox_TextChanged(object sender, EventArgs e) {
            _deviceName = deviceNameTextBox.Text;
        }

        private void deviceNameTextBox_Leave(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(deviceNameTextBox.Text.Trim()))
                deviceNameTextBox.Text = _deviceNamePlaceHolder;
        }

        /************************ vendorIdTextBox ************************/
        private void vendorIdTextBox_Enter(object sender, EventArgs e) {
            if (_clearPlaceholder && _vendorIdPlaceHolder == vendorIdTextBox.Text)
                vendorIdTextBox.Text = "";
        }

        private void vendorIdTextBox_TextChanged(object sender, EventArgs e) {
            _vendorId = vendorIdTextBox.Text;
        }

        private void vendorIdTextBox_Leave(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(vendorIdTextBox.Text.Trim()))
                vendorIdTextBox.Text = _vendorIdPlaceHolder;
        }

        private void vendorIdTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e) {
            if (_DEFAULT_PLACEHOLDER_VENDOR_ID == _vendorId) {
                _showInvalidIdErrorMessage("vendor");
                e.Cancel = true;
                return;
            }

            if (!Regex.IsMatch(_vendorId, @"^[A-Z0-9]+$")) {
                _showInvalidIdErrorMessage("vendor");
                e.Cancel = true;
                return;
            }

            e.Cancel = false;
        }

        /************************ productIdTextBox ************************/
        private void productIdTextBox_Enter(object sender, EventArgs e) {
            if (_clearPlaceholder && _productIdPlaceHolder == productIdTextBox.Text)
                productIdTextBox.Text = "";
        }

        private void productIdTextBox_TextChanged(object sender, EventArgs e) {
            _productId = productIdTextBox.Text;
        }

        private void productIdTextBox_Leave(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(productIdTextBox.Text.Trim()))
                productIdTextBox.Text = _productIdPlaceHolder;
        }

        private void productIdTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e) {
            if (_DEFAULT_PLACEHOLDER_PRODUCT_ID == _productId) {
                _showInvalidIdErrorMessage("product");
                e.Cancel = true;
                return;
            }

            if (!Regex.IsMatch(_productId, @"^[A-Z0-9]+$")) {
                _showInvalidIdErrorMessage("product");
                e.Cancel = true;
                return;
            }

            e.Cancel = false;
        }

        #endregion

        #endregion

        #region Public Methods

        public DialogResult show(string title) {
            Text = title;
            return ShowDialog();
        }

        #endregion

    }
}
