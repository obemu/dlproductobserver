﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

using DLProductObserver.Backend;
using DLProductObserver.Backend.Model;
using System.Collections;

namespace DLProductObserver.UI.Dialog {
    public partial class SettingsDialog : Form {

        #region Private Declarations

        private const string LOG_TAG = nameof(SettingsDialog);

        #endregion

        #region Public Declarations

        #endregion

        public SettingsDialog() {
            Load += settingsDialog_Load;

            InitializeComponent();
            _updateListView();
            _sortListView();

            outputDirectoryTextBox.Text = Settings.outputDirectoryPath;
            Select();
        }

        #region Private Methods

        private void _sortListView() {
            deviceEntityListView.Sorting = Settings.listViewSortingOrder;
            deviceEntityListView.ListViewItemSorter = new _DeviceEntityListViewItemComparer(
                    Settings.deviceListViewSortingColumn
                );
        }

        private static DeviceEntity _deviceEntityFromListViewItem(ListViewItem item)
             => new DeviceEntity(
                 item.SubItems[0].Text,
                 item.SubItems[1].Text,
                 item.SubItems[2].Text,
                 item.Checked
                 );

        private void _updateListView() {

            deviceEntityListView.BeginUpdate();

            deviceEntityListView.SelectedItems.Clear();
            deviceEntityListView.Items.Clear();

            foreach (var entity in Settings.deviceEntities) {
                ListViewItem item = new ListViewItem(entity.simpleName ?? "");

                item.SubItems.Add(entity.vendorId ?? "null");
                item.SubItems.Add(entity.productId ?? "null");
                item.Checked = entity.isEnabled;

                deviceEntityListView.Items.Add(item);
            }

            deviceEntityListView.EndUpdate();

            Settings.save();

            _sortListView();
        }

        private DeviceEntity _getSelectedDeviceEntity() {
            if (0 == deviceEntityListView.SelectedItems.Count)
                return null;

            return _deviceEntityFromListViewItem(deviceEntityListView.SelectedItems[0]);
        }

        #region Eventhandlers

        private void settingsDialog_Load(object sender, EventArgs e) {
            outputDirectoryTextBox.BackColor = SystemColors.Window;
            outputDirectoryTextBox.ForeColor = SystemColors.WindowText;
        }

        #endregion

        #region UI Eventhandlers

        private void deviceEntityListView_ItemChecked(object sender, ItemCheckedEventArgs e) {
            var idx = Settings.deviceEntities.indexOf(_deviceEntityFromListViewItem(e.Item));

            if (0 > idx)
                return;

            Settings.deviceEntities[idx].isEnabled = e.Item.Checked;
        }

        private void deviceEntityListView_ColumnClick(object sender, ColumnClickEventArgs e) {
            Settings.deviceListViewSortingColumn = e.Column;
            _sortListView();
        }

        private void addDeviceButton_Click(object sender, EventArgs e) {
            var dialog = new DeviceDialog();

            if (DialogResult.OK == dialog.show("Add Device")) {

                if (!Settings.deviceEntities.contains(dialog.deviceEntity)) {
                    Settings.deviceEntities.add(dialog.deviceEntity);
                    _updateListView();
                }
            }
        }

        private void editDeviceButton_Click(object sender, EventArgs e) {
            var entity = _getSelectedDeviceEntity();
            if (null == entity)
                return;

            var idx = Settings.deviceEntities.indexOf(entity);
            if (0 > idx)
                return;

            var dialog = new DeviceDialog(entity, false);
            if (DialogResult.OK == dialog.show("Edit Device")) {
                Settings.deviceEntities[idx] = dialog.deviceEntity;
                _updateListView();
            }
        }

        private void removeDeviceButton_Click(object sender, EventArgs e) {
            if (Settings.deviceEntities.remove(_getSelectedDeviceEntity()))
                _updateListView();
        }

        private void chooseDirectoryButton_Click(object sender, EventArgs e) {
            var dialog = new FolderBrowserDialog();
            if (DialogResult.OK == dialog.ShowDialog()
                && Settings.outputDirectoryPath != dialog.SelectedPath) {

                if (Directory.Exists(dialog.SelectedPath)) {
                    Settings.outputDirectoryPath = dialog.SelectedPath;
                    outputDirectoryTextBox.Text = dialog.SelectedPath;
                } else {
                    Utils.showErrorMessage(
                        "DL Product Observer",
                        "The directory '" + dialog.SelectedPath + "' does not exist!"
                        );
                }
            }
        }

        #endregion

        #endregion

        #region Public Methods

        public DialogResult show() {
            return ShowDialog();
        }

        #endregion

        #region Class Definition _DeviceEntityListViewItemComparer

        private class _DeviceEntityListViewItemComparer : IComparer {

            #region Private Declarations

            private readonly int _column;

            #endregion

            public _DeviceEntityListViewItemComparer(int column) {
                this._column = column;
            }

            public _DeviceEntityListViewItemComparer() : this(0) { }

            #region Public Methods

            public int Compare(object x, object y) {
                var entityX = _deviceEntityFromListViewItem(x as ListViewItem);
                var entityY = _deviceEntityFromListViewItem(y as ListViewItem);

                switch (_column) {
                    default:
                    case 0:
                        return string.Compare(entityX.simpleName, entityY.simpleName);

                    case 1:
                        return string.Compare(entityX.vendorId, entityY.vendorId);

                    case 2:
                        return string.Compare(entityX.productId, entityY.productId);
                }
            }

            #endregion
        }

        #endregion
    }
}
