﻿
namespace DLProductObserver.UI {
    partial class MainPage {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.menuButtonsFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.addItemButton = new DLProductObserverControls.NoFocusCueButton();
            this.editItemButton = new DLProductObserverControls.NoFocusCueButton();
            this.removeItemButton = new DLProductObserverControls.NoFocusCueButton();
            this.settingsFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.settingsButton = new DLProductObserverControls.NoFocusCueButton();
            this.productListView = new DLProductObserverControls.NoFocusCueListView();
            this.productColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.barcodeColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.priceColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tableLayoutPanelTextBox = new System.Windows.Forms.TableLayoutPanel();
            this.timelineTextBox = new DLProductObserverControls.NoFocusCueTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.menuButtonsFlowLayoutPanel.SuspendLayout();
            this.settingsFlowLayoutPanel.SuspendLayout();
            this.tableLayoutPanelTextBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.menuButtonsFlowLayoutPanel, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.settingsFlowLayoutPanel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.productListView, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanelTextBox, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(923, 467);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // menuButtonsFlowLayoutPanel
            // 
            this.menuButtonsFlowLayoutPanel.Controls.Add(this.addItemButton);
            this.menuButtonsFlowLayoutPanel.Controls.Add(this.editItemButton);
            this.menuButtonsFlowLayoutPanel.Controls.Add(this.removeItemButton);
            this.menuButtonsFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuButtonsFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.menuButtonsFlowLayoutPanel.Location = new System.Drawing.Point(279, 422);
            this.menuButtonsFlowLayoutPanel.Name = "menuButtonsFlowLayoutPanel";
            this.menuButtonsFlowLayoutPanel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuButtonsFlowLayoutPanel.Size = new System.Drawing.Size(641, 42);
            this.menuButtonsFlowLayoutPanel.TabIndex = 3;
            // 
            // addItemButton
            // 
            this.addItemButton.Location = new System.Drawing.Point(553, 3);
            this.addItemButton.Name = "addItemButton";
            this.addItemButton.Size = new System.Drawing.Size(85, 30);
            this.addItemButton.TabIndex = 1;
            this.addItemButton.Text = "Add";
            this.addItemButton.UseVisualStyleBackColor = true;
            this.addItemButton.Click += new System.EventHandler(this.addItemButton_Click);
            // 
            // editItemButton
            // 
            this.editItemButton.Location = new System.Drawing.Point(462, 3);
            this.editItemButton.Name = "editItemButton";
            this.editItemButton.Size = new System.Drawing.Size(85, 30);
            this.editItemButton.TabIndex = 2;
            this.editItemButton.Text = "Edit";
            this.editItemButton.UseVisualStyleBackColor = true;
            this.editItemButton.Click += new System.EventHandler(this.editItemButton_Click);
            // 
            // removeItemButton
            // 
            this.removeItemButton.Location = new System.Drawing.Point(371, 3);
            this.removeItemButton.Name = "removeItemButton";
            this.removeItemButton.Size = new System.Drawing.Size(85, 30);
            this.removeItemButton.TabIndex = 3;
            this.removeItemButton.Text = "Remove";
            this.removeItemButton.UseVisualStyleBackColor = true;
            this.removeItemButton.Click += new System.EventHandler(this.removeItemButton_Click);
            // 
            // settingsFlowLayoutPanel
            // 
            this.settingsFlowLayoutPanel.Controls.Add(this.settingsButton);
            this.settingsFlowLayoutPanel.Location = new System.Drawing.Point(3, 422);
            this.settingsFlowLayoutPanel.Name = "settingsFlowLayoutPanel";
            this.settingsFlowLayoutPanel.Size = new System.Drawing.Size(270, 42);
            this.settingsFlowLayoutPanel.TabIndex = 5;
            // 
            // settingsButton
            // 
            this.settingsButton.Location = new System.Drawing.Point(3, 3);
            this.settingsButton.Name = "settingsButton";
            this.settingsButton.Size = new System.Drawing.Size(85, 30);
            this.settingsButton.TabIndex = 0;
            this.settingsButton.Text = "Settings";
            this.settingsButton.UseVisualStyleBackColor = true;
            this.settingsButton.Click += new System.EventHandler(this.settingsButton_Click);
            // 
            // productListView
            // 
            this.productListView.AllowColumnReorder = true;
            this.productListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.productColumn,
            this.barcodeColumn,
            this.priceColumn});
            this.productListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.productListView.FullRowSelect = true;
            this.productListView.GridLines = true;
            this.productListView.HideSelection = false;
            this.productListView.Location = new System.Drawing.Point(279, 3);
            this.productListView.MultiSelect = false;
            this.productListView.Name = "productListView";
            this.productListView.Size = new System.Drawing.Size(641, 395);
            this.productListView.TabIndex = 6;
            this.productListView.TabStop = false;
            this.productListView.UseCompatibleStateImageBehavior = false;
            this.productListView.View = System.Windows.Forms.View.Details;
            this.productListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.productListView_ColumnClick);
            // 
            // productColumn
            // 
            this.productColumn.Text = "Product";
            this.productColumn.Width = 180;
            // 
            // barcodeColumn
            // 
            this.barcodeColumn.Text = "Barcode";
            this.barcodeColumn.Width = 180;
            // 
            // priceColumn
            // 
            this.priceColumn.Text = "Price";
            this.priceColumn.Width = 180;
            // 
            // tableLayoutPanelTextBox
            // 
            this.tableLayoutPanelTextBox.ColumnCount = 1;
            this.tableLayoutPanelTextBox.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelTextBox.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelTextBox.Controls.Add(this.timelineTextBox, 0, 0);
            this.tableLayoutPanelTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelTextBox.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelTextBox.Name = "tableLayoutPanelTextBox";
            this.tableLayoutPanelTextBox.RowCount = 1;
            this.tableLayoutPanelTextBox.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelTextBox.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 395F));
            this.tableLayoutPanelTextBox.Size = new System.Drawing.Size(270, 395);
            this.tableLayoutPanelTextBox.TabIndex = 8;
            // 
            // timelineTextBox
            // 
            this.timelineTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.timelineTextBox.Location = new System.Drawing.Point(3, 3);
            this.timelineTextBox.Multiline = true;
            this.timelineTextBox.Name = "timelineTextBox";
            this.timelineTextBox.ReadOnly = true;
            this.timelineTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.timelineTextBox.ShortcutsEnabled = false;
            this.timelineTextBox.Size = new System.Drawing.Size(264, 389);
            this.timelineTextBox.TabIndex = 8;
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 467);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainPage";
            this.Text = "Defence Lab Product Observer";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.menuButtonsFlowLayoutPanel.ResumeLayout(false);
            this.settingsFlowLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanelTextBox.ResumeLayout(false);
            this.tableLayoutPanelTextBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DLProductObserverControls.NoFocusCueButton addItemButton;
        private System.Windows.Forms.FlowLayoutPanel menuButtonsFlowLayoutPanel;
        private DLProductObserverControls.NoFocusCueButton editItemButton;
        private DLProductObserverControls.NoFocusCueButton removeItemButton;
        private System.Windows.Forms.FlowLayoutPanel settingsFlowLayoutPanel;
        private DLProductObserverControls.NoFocusCueButton settingsButton;
        private DLProductObserverControls.NoFocusCueListView productListView;
        private System.Windows.Forms.ColumnHeader productColumn;
        private System.Windows.Forms.ColumnHeader barcodeColumn;
        private System.Windows.Forms.ColumnHeader priceColumn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTextBox;
        private DLProductObserverControls.NoFocusCueTextBox timelineTextBox;
    }
}

