﻿using System;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using KbdListener;
using DLProductObserver.UI.Dialog;
using DLProductObserver.Backend;
using DLProductObserver.Backend.Model;
using System.Collections.Generic;
using System.Collections;

namespace DLProductObserver.UI {
    public partial class MainPage : Form {

        #region Private Declarations

        private const string _LOG_TAG = nameof(MainPage);

        private readonly object _THREAD_LOCK = new object();

        private volatile StringBuilder _builder;
        private Thread _backgroundThread;

        private KeyboardListener.KeyboardDevice _lastKeyboardDevice;

#if DEBUG
        private DLProductObserverControls.NoFocusCueButton _testScanButton;
#endif

        #endregion

        #region Public Declarations

        public event EventHandler<ProductEventArgs> barcodeScanned;
        public event EventHandler<KeyboardDeviceEventArgs> keyboardDeviceAdded;

        public List<KeyboardListener.KeyboardDevice> keyboardDevices;

        public bool isProductDialogActive {
            get; private set;
        }

        #endregion

        public MainPage() {
            keyboardDevices = new List<KeyboardListener.KeyboardDevice>();
            _builder = new StringBuilder();
            _backgroundThread = new Thread(new ThreadStart(_backgroundThreadLoop)) { IsBackground = true };
            isProductDialogActive = false;

            Load += mainPage_Load;
            barcodeScanned += mainPage_barcodeScanned;

            InitializeComponent();
            _updateListView();
            _sortListView();

#if DEBUG
            _testScanButton = new DLProductObserverControls.NoFocusCueButton();
            _testScanButton.Location = new System.Drawing.Point(290, 3);
            _testScanButton.Name = "testScanButton";
            _testScanButton.Size = new System.Drawing.Size(75, 23);
            _testScanButton.TabIndex = 4;
            _testScanButton.Text = "testScan";
            _testScanButton.UseVisualStyleBackColor = true;
            _testScanButton.Click += new EventHandler(testScanButton_Click);
            menuButtonsFlowLayoutPanel.Controls.Add(_testScanButton);
#endif
        }

        #region Private methods

        private void _sortListView() {
            productListView.Sorting = Settings.listViewSortingOrder;
            productListView.ListViewItemSorter = new _ProductListViewItemComparer(
                     Settings.productListViewSortingColumn
                );
        }

        private void _updateListView() {
            productListView.BeginUpdate();

            productListView.SelectedItems.Clear();
            productListView.Items.Clear();

            Settings.products.forEach((product) => {
                ListViewItem item = new ListViewItem(product.name);
                var price = ((double)product.priceInCent) / 100;
                item.SubItems.Add(product.barcode);
                item.SubItems.Add(price.ToString());
                productListView.Items.Add(item);
            });

            productListView.EndUpdate();

            Settings.save();

            _sortListView();
        }

        private void _backgroundThreadLoop() {
            ProductEventArgs args = null;
            MethodInvoker onBarcodeScannedInvoker = delegate {
                onBarcodeScanned(args);
            };

            while (true) {
                if (0 != _builder.Length) {
                    lock (_THREAD_LOCK) {
                        Product found = Settings.products.find(_builder.ToString());

                        if (null != found) {
                            args = new ProductEventArgs(found, DateTime.Now, _lastKeyboardDevice);
                            Invoke(onBarcodeScannedInvoker);
                            _builder.Clear();
                        }
                    }
                }

                Thread.Sleep(100);
            }
        }

        private int _getSelectedProductIndex() {
            if (0 == productListView.SelectedItems.Count)
                return -1;

            ListViewItem selected = productListView.SelectedItems[0];
            var found = Settings.products.find(selected.SubItems[1].Text);

            return Settings.products.indexOf(found);
        }

        #region Eventhandlers

        private void mainPage_Load(object sender, EventArgs e) {
            // Watch for keyboard activity
            KeyboardListener.keyEventHandler += keyboardListener_s_KeyEventHandler;

            _backgroundThread.Start();
        }

        private void mainPage_barcodeScanned(object sender, ProductEventArgs args) {
            if (null == Settings.deviceEntities.find(
                 Utils.getPNPEntityFromKeyboardDevice(args.keyboardDevice)
                ))
                return;

            var currentLine = $"{Utils.getShortTimeString(args.productEvent.timepoint)}\t{args.productEvent.product.name}";
            var lines = new string[timelineTextBox.Lines.Length + 1];

            lines[0] = currentLine;
            Array.Copy(timelineTextBox.Lines, 0, lines, 1, timelineTextBox.Lines.Length);

            timelineTextBox.Lines = lines;
        }

        private void keyboardListener_s_KeyEventHandler(object sender, EventArgs e) {
            if (isProductDialogActive)
                return;

            KeyboardListener.UniversalKeyEventArgs args = (KeyboardListener.UniversalKeyEventArgs)e;

            if (WindowsMessage.KEYDOWN == args.message) {

                _lastKeyboardDevice = args.keyboardDevice;

                if (!keyboardDevices.Contains(args.keyboardDevice)) {
                    keyboardDevices.Add(args.keyboardDevice);
                    onKeyboardDeviceAdded(new KeyboardDeviceEventArgs(args.keyboardDevice));
                }

                lock (_THREAD_LOCK) {
                    _builder.Append(args.keyToString());
                }
            }
        }

        #endregion

        #region UI Eventhandlers

        private void addItemButton_Click(object sender, EventArgs e) {
            isProductDialogActive = true;

            var dialog = new ProductDialog();
            if (DialogResult.OK == dialog.show("Add Item")) {
                var product = dialog.product;
                if (!Settings.products.contains(product)) {
                    Settings.products.add(product);
                    _updateListView();
                }
            }

            isProductDialogActive = false;
        }

        private void editItemButton_Click(object sender, EventArgs e) {
            int index = _getSelectedProductIndex();

            if (0 > index)
                return;

            isProductDialogActive = true;

            var dialog = new ProductDialog(Settings.products[index], false);
            if (DialogResult.OK == dialog.show("Edit Item")) {
                Settings.products[index] = dialog.product;
                _updateListView();
            }

            isProductDialogActive = false;
        }

        private void removeItemButton_Click(object sender, EventArgs e) {
            int index = _getSelectedProductIndex();

            if (0 > index)
                return;

            Settings.products.removeAt(index);
            _updateListView();
        }

        private void settingsButton_Click(object sender, EventArgs e) {
            new SettingsDialog().show();
        }

#if DEBUG
        private void testScanButton_Click(object sender, EventArgs e) {
            if (0 == Settings.products.Count) {
                Log.d(nameof(testScanButton_Click), "Settings.products.Count equals 0");
                return;
            }

            if (0 == Settings.deviceEntities.Count) {
                Log.d(nameof(testScanButton_Click), "Settings.deviceEntities.Count equals 0");
                return;
            }

            var random = new Random();

            var randomProduct = Settings.products[random.Next(Settings.products.Count)];
            var timePoint = DateTime.Now;

            Log.d(nameof(testScanButton_Click),
                    "product: " + randomProduct,
                    "timePoint: " + timePoint,
                    "device: " + null
                );

            onBarcodeScanned(new ProductEventArgs(
                        randomProduct,
                        timePoint,
                        null
                    ));
        }
#endif

        private void productListView_ColumnClick(object sender, ColumnClickEventArgs e) {
            Settings.productListViewSortingColumn = e.Column;
            _sortListView();
        }

        #endregion

        #endregion

        #region Protected Methods

        protected virtual void onBarcodeScanned(ProductEventArgs e) {
            EventHandler<ProductEventArgs> handler = barcodeScanned;
            handler?.Invoke(this, e);
        }

        protected virtual void onKeyboardDeviceAdded(KeyboardDeviceEventArgs e) {
            EventHandler<KeyboardDeviceEventArgs> handler = keyboardDeviceAdded;
            handler?.Invoke(this, e);
        }

        #endregion

        #region Class Definition KeyboardDeviceEventArgs

        public class KeyboardDeviceEventArgs : EventArgs {
            public KeyboardListener.KeyboardDevice keyboardDevice {
                get; private set;
            }

            public KeyboardDeviceEventArgs(KeyboardListener.KeyboardDevice keyboardDevice) {
                this.keyboardDevice = keyboardDevice;
            }
        }

        #endregion

        #region Class Definition _ProductListViewItemComparer

        private class _ProductListViewItemComparer : IComparer {

            #region Private Declarations

            private readonly int column;

            #endregion

            public _ProductListViewItemComparer(int column) {
                this.column = column;
            }

            public _ProductListViewItemComparer() : this(0) { }

            #region Public Methods

            public int Compare(object x, object y) {
                var productX = new Product(
                        (x as ListViewItem).SubItems[0].Text,
                        (x as ListViewItem).SubItems[1].Text,
                        (x as ListViewItem).SubItems[2].Text
                    );
                var productY = new Product(
                        (y as ListViewItem).SubItems[0].Text,
                        (y as ListViewItem).SubItems[1].Text,
                        (y as ListViewItem).SubItems[2].Text
                    );

                switch (column) {
                    default:
                    case 0:
                        return string.Compare(productX.name, productY.name);

                    case 1:
                        return string.Compare(productX.barcode, productY.barcode);

                    case 2:
                        return productX.priceInCent - productY.priceInCent;
                }
            }

            #endregion
        }

        #endregion
    }
}
